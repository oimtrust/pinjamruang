<?php
include_once 'Connection.php';

class Mastering extends Connection
{
    public function validateSchedule($id_ruang, $id_hari, $tanggal_pinjam, $jam_awal, $jam_akhir) {
        $jam_awal = date("H:i:s", strtotime("+1 minutes", strtotime($jam_awal)));
        $jam_akhir = date("H:i:s", strtotime("-1 minutes", strtotime($jam_akhir)));
        $result = $this->db->query("SELECT * FROM tbl_pinjaman
                WHERE 
                (
                  (jam_awal BETWEEN '{$jam_awal}' AND '{$jam_akhir}')
                  OR
                  (jam_akhir BETWEEN '{$jam_awal}' AND '{$jam_akhir}')
                )
                  AND
                  tanggal_pinjam = '{$tanggal_pinjam}' AND id_hari = '{$id_hari}' AND id_ruang = '{$id_ruang}'");

        return $result->fetch_assoc();
    }

    public function createBorrowAccepted($id_peminjam, $id_hari, $tanggal_pinjam, $jam_awal, $jam_akhir, $id_ruang, $id_acara, $keterangan)
    {
        $result = $this->db->query(
            "INSERT INTO tbl_pinjaman (id_peminjam, id_hari, tanggal_pinjam, jam_awal, jam_akhir, id_ruang, id_acara, keterangan, `status`, alasan)
                  VALUES ('{$id_peminjam}', '{$id_hari}', '{$tanggal_pinjam}', '{$jam_awal}', '{$jam_akhir}', '{$id_ruang}', '{$id_acara}', '{$keterangan}', 'DITERIMA', 'Peminjaman anda pada tanggal $tanggal_pinjam dari mulai jam $jam_awal hingga jam $jam_akhir tidak ada peminjaman')"
        );
    }

    public function setRoomToUse($id_ruang)
    {
        $result = $this->db->query("UPDATE tbl_ruang SET status = 'TERPAKAI' WHERE id_ruang = '{$id_ruang}'");
    }

    public function createBorrowDenied($id_peminjam, $id_hari, $tanggal_pinjam, $jam_awal, $jam_akhir, $id_ruang, $id_acara, $keterangan)
    {
        $result = $this->db->query(
            "INSERT INTO tbl_pinjaman (id_peminjam, id_hari, tanggal_pinjam, jam_awal, jam_akhir, id_ruang, id_acara, keterangan, `status`, alasan)
                  VALUES ('{$id_peminjam}', '{$id_hari}', '{$tanggal_pinjam}', '{$jam_awal}', '{$jam_akhir}', '{$id_ruang}', '{$id_acara}', '{$keterangan}', 'DITOLAK', 'Peminjaman anda pada tanggal $tanggal_pinjam dari mulai jam $jam_awal hingga jam $jam_akhir ada jadwal peminjaman ruang')"
        );
    }

    public function updateBorrowAccepted($id_hari, $tanggal_pinjam, $jam_awal, $jam_akhir, $id_ruang, $id_acara, $keterangan, $id_pinjaman)
    {
        $result = $this->db->query(
            "UPDATE tbl_pinjaman SET id_hari = '{$id_hari}', tanggal_pinjam = '{$tanggal_pinjam}', jam_awal = '{$jam_awal}', jam_akhir = '{$jam_akhir}', id_ruang = '{$id_ruang}', id_acara = '{$id_acara}', keterangan = '{$keterangan}',
                    status = 'DITERIMA', alasan = 'Perubahan jadwal peminjaman anda pada tanggal $tanggal_pinjam dari mulai jam $jam_awal hingga jam $jam_akhir diterima' WHERE id_pinjaman = '{$id_pinjaman}'");
    }

    public function updateBorrowDenied($id_hari, $tanggal_pinjam, $jam_awal, $jam_akhir, $id_ruang, $id_acara, $keterangan, $id_pinjaman)
    {
        $result = $this->db->query(
            "UPDATE tbl_pinjaman SET id_hari = '{$id_hari}', tanggal_pinjam = '{$tanggal_pinjam}', jam_awal = '{$jam_awal}', jam_akhir = '{$jam_akhir}', id_ruang = '{$id_ruang}', id_acara = '{$id_acara}', keterangan = '{$keterangan}',
                    status = 'DITERIMA', alasan = 'Perubahan jadwal peminjaman anda pada tanggal $tanggal_pinjam dari mulai jam $jam_awal hingga jam $jam_akhir diterima' WHERE id_pinjaman = '{$id_pinjaman}'");
    }

    public function deleteBorrow($id_pinjaman)
    {
        $result = $this->db->query("DELETE FROM tbl_pinjaman WHERE id_pinjaman = '{$id_pinjaman}'");
    }
}