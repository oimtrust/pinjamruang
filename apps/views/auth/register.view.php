<div class="row">
    <div class="col s12 z-depth-6 card-panel">
        <!-- Start Alert box -->
            <?php
            if (isset($error)) {
                foreach ($error as $error) {
                    ?>
                    <div class="row alert_box">
                        <div class="col s12 m12">
                            <div class="card red darken-2">
                                <div class="row">
                                    <div class="col s9">
                                        <div class="card-content white-text">
                                            <p><?php echo $error; ?></p>
                                        </div>
                                    </div>
                                    <div class="col s3 white-text">
                                        <i class="mdi mdi-close close right alert_close" aria-hidden="true"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php

                }
            } elseif (isset($_GET['registered'])) {
                ?>
                <div class="row alert_box">
                    <div class="col s12">
                        <div class="card green darken-2">
                            <div class="row">
                                <div class="col s9">
                                    <div class="card-content white-text">
                                        Selamat! Pendaftaran anda berhasil.
                                        Anda bisa melakukan login <a href="<?php $baseUrl; ?>index.php?page=auth&action=login" class="yellow-text">Disini</a>
                                    </div>
                                </div>
                                <div class="col s3 white-text">
                                    <i class="mdi mdi-close close right alert_close" aria-hidden="true"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php

            }
            ?>
            <!-- End Alert box -->
        <form class="login-form" method="post">
            <div class="row">
                <div class="input-field col s12 center">
                    <a href="#">
                        <img src="<?php $baseUrl;?>public/img/logo.png" alt="" class="responsive-img valign profile-image-login">
                    </a>
                </div>
            </div>
            <div class="row">
                <div class="input-field col s12">
                    <i class="mdi mdi-account-circle prefix"></i>
                    <input id="code_user" name="code_user" type="text" onkeypress="return onlyNumber(event)" class="validate" autofocus>
                    <label for="code_user">NIM/NIK/NIDN</label>
                </div>

                <div class="input-field col s12">
                    <i class="mdi mdi-account-circle prefix"></i>
                    <input id="username" name="username" type="text" class="validate" autofocus>
                    <label for="username">Username</label>
                </div>
                <div class="input-field col s12">
                    <i class="mdi mdi-key-variant prefix"></i>
                    <input id="password" name="password" type="password" class="validate">
                    <label for="password">Password</label>
                </div>

                <div class="input-field col s12">
                    <i class="mdi mdi-face prefix"></i>
                    <input id="fullname" name="fullname" type="text" class="validate" >
                    <label for="fullname">Nama Lengkap</label>
                </div>

                <div class="input-field col s12">
                    <i class="mdi mdi-cellphone-android prefix"></i>
                    <input id="phone" name="phone" type="text" onkeypress="return onlyNumber(event)" class="validate" >
                    <label for="phone">No. Telepon/HP</label>
                </div>

                <div class="input-field col s12">
                    <i class="mdi mdi-email-outline prefix"></i>
                    <input id="email" name="email" type="text" class="validate" >
                    <label for="email">Email</label>
                </div>

                <div class="input-field col s12">
                    <i class="mdi mdi-home-outline prefix"></i>
                    <textarea id="address" name="address"class="materialize-textarea textarea"></textarea>
                    <label for="address">Alamat</label>
                </div>

                <div class="input-field col s12">
                    <i class="mdi mdi-account-settings-variant prefix"></i>
                    <select name="status">
                        <option value="" disabled selected>Pilih Status</option>
                        <option value="Karyawan">Karyawan</option>
                        <option value="Dosen">Dosen</option>
                        <option value="Mahasiswa">Mahasiswa</option>
                    </select>
                    <label>Status</label>
                </div>

                <div class="input-field col s12">
                    <i class="mdi mdi-account-multiple-outline prefix"></i>
                    <?php
                    $id_instansi = $connect->execute("SELECT * FROM tbl_instansi WHERE id_instansi ORDER BY id_instansi ASC");
                    $row_count = $id_instansi->num_rows;
                    ?>
                    <select name="id_instansi" class="id_instansi">
                        <option value="" disabled="disabled" selected>Pilih Instansi</option>
                        <?php
                        if ($row_count > 0) {
                            while ($row = $id_instansi->fetch_object()) {
                                echo '<option value="' . $row->id_instansi . '">' . $row->nama_instansi . '</option>';
                            }
                        } else {
                            echo '<option value="">Nama instansi tidak tersedia</option>';
                        }
                        ?>
                    </select>
                    <label>Pilih Instansi</label>
                </div>
            </div>
            <div class="row">
                <button type="submit" name="btn_register" class="btn waves-effect waves-light deep-purple darken-4 lighten-1 col s12"><i class="mdi mdi-account-edit small"></i> Daftar</button>
            </div>

            <div class="row">
                <span>
                    Anda sudah punya akun? <a href="<?php $baseUrl;?>index.php?page=auth&action=login">Masuk Disini</a> 
                </span>
            </div>
        </form>
    </div>
</div>

<style type="text/css">
    /*
 * Just for auth design CSS
 */

    html,
    body {
        background-color: #ff6f00;
        height: 100%;
    }
    html {
        display: table;
        margin: auto;
    }
    body {
        display: table-cell;
        vertical-align: middle;
    }
    .margin {
        margin: 0 !important;
    }
</style>

<script type="text/javascript">
    $('.alert_close').click(function(){
        $( ".alert_box" ).fadeOut( "slow", function() {
        });
    });

    $('.textarea').trigger('autoresize');

    $('select').material_select();

    function onlyNumber(evt) {
		  var charCode = (evt.which) ? evt.which : event.keyCode
		   if (charCode > 31 && (charCode < 48 || charCode > 57))
 
		    return false;
		  return true;
		}
</script>