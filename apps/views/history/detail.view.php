<div class="container">
    <form method="post">
        <div class="row">
            <div class="col s12">
            <div class="card">
                <div class="card-image">
                <img src="<?php $baseUrl; ?>public/img/laptop.jpg">
                <span class="card-title"><?php echo $detailPinjam->fullname; ?></span>
                </div>
                <div class="card-content">
                    <h5><?php echo $detailPinjam->nama_acara; ?></h5>
                    <p>
                        <?php echo $detailPinjam->keterangan; ?>
                    </p>
                </div>
            </div>

            <div class="card-panel  orange lighten-5">
                <span>INSTANSI</span>
                <h5><?php echo $detailPinjam->nama_instansi; ?></h5>
            </div>

            <div class="card-panel  orange lighten-4">
                <span>RUANG YANG DIPINJAM</span>
                <h5><?php echo $detailPinjam->nama_ruang; ?></h5>
            </div>

            <div class="card-panel  orange lighten-3">
                <span>TANGGAL PEMINJAMAN</span>
                <h5><?php echo $detailPinjam->tanggal_pinjam; ?></h5>
            </div>

            <div class="card-panel orange lighten-2">
                <span>WAKTU</span>
                <h5><?php echo $detailPinjam->jam_awal; ?> Hingga <?php echo $detailPinjam->jam_akhir;?></h5>
            </div>

            <div class="card-panel orange lighten-1">
                <span>Alasan</span>
                <h5><?php echo $detailPinjam->alasan; ?></h5>
            </div>
            </div>
        </div>

        <div class="fixed-action-btn vertical click-to-toggle">
            <a class="btn-floating btn-large red">
            <i class="mdi mdi-menu"></i>
            </a>
            <ul>
            <li><a href="<?php $baseUrl;?>index.php?page=home&action=delete&delete_id=<?php echo $detailPinjam->id_pinjaman;?>" class="btn-floating red tooltip btn-delete" title="Hapus"><i class="mdi mdi-delete-empty"></i></a></li>
            <li><a href="<?php $baseUrl;?>index.php?page=home&action=update&update_id=<?php echo $detailPinjam->id_pinjaman;?>" class="btn-floating blue tooltip" title="Ubah"><i class="mdi mdi-pencil"></i></a></li>
            </ul>
        </div>
    </form>
</div>

<script>
    tippy('.tooltip', {
        placement: 'left',
        animation: 'scale',
        theme: 'menu light',
        trigger: 'click',
        duration: 1000,
        arrow: true
    })

    $('.btn-delete').on('click',function(){
        var getLink = $(this).attr('href');

        swal({
            title: 'Hapus Data Peminjaman',
            text: 'Loe Yakin Broo?',
            html: true,
            confirmButtonColor: '#d9534f',
            showCancelButton: true,
        },function(){
            window.location.href = getLink
        });

        return false;
    });
</script>