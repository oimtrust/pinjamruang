<div class="container">
    <div class="row">
        <?php
        if (isset($_GET['accepted'])) {
        ?>
        <div class="row alert_box">
            <div class="col s12">
                <div class="card green darken-2">
                    <div class="row">
                        <div class="col s9">
                            <div class="card-content white-text">
                                Selamat! Peminjaman DITERIMA!
                            </div>
                        </div>
                        <div class="col s3 white-text">
                            <i class="mdi mdi-close close right alert_close" aria-hidden="true"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php
    } elseif (isset($_GET['denied'])) {
    ?>
    <div class="row alert_box">
        <div class="col s12">
            <div class="card orange darken-2">
                <div class="row">
                    <div class="col s9">
                        <div class="card-content white-text">
                            Mohon Maaf! Peminjaman DITOLAK!
                        </div>
                    </div>
                    <div class="col s3 white-text">
                        <i class="mdi mdi-close close right alert_close" aria-hidden="true"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php

    } elseif (isset($_GET['deleted'])) {
        ?>
        <div class="row alert_box">
            <div class="col s12">
                <div class="card orange darken-2">
                    <div class="row">
                        <div class="col s9">
                            <div class="card-content white-text">
                                Data Peminjaman Berhasil Dihapus!!
                            </div>
                        </div>
                        <div class="col s3 white-text">
                            <i class="mdi mdi-close close right alert_close" aria-hidden="true"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php

    }
    ?>
    </div>
</div>

<div class="collection">
    <?php
    $query = $connect->execute(
            "SELECT
                pinjam.id_pinjaman,
                pinjam.id_peminjam,
                pinjam.status,
                pinjam.tanggal_pinjam,
                pinjam.id_ruang,
                ruang.nama_ruang,
                peminjam.username,
                acara.id_acara,
                acara.nama_acara
            FROM tbl_pinjaman AS pinjam 
            LEFT JOIN tbl_ruang AS ruang ON pinjam.id_ruang = ruang.id_ruang
            LEFT JOIN tbl_peminjam AS peminjam ON pinjam.id_peminjam = peminjam.id_peminjam
            LEFT JOIN tbl_acara AS acara ON pinjam.id_acara = acara.id_acara
            WHERE peminjam.username = '{$user_login}' ORDER BY pinjam.updated_at DESC");
 
    while ($data = $query->fetch_object()) {
        if ($data->status == 'MENUNGGU') {
            $color = "amber darken-1";
        }
        elseif ($data->status == 'DITERIMA') {
            $color = "green darken-3";
        }
        else {
            $color = "red darken-1";
        }

        ?>
        <a href="<?php $baseUrl;?>index.php?page=home&action=detail&detail_id=<?php echo $data->id_pinjaman;?>" class="collection-item <?php echo $color;?> white-text"><b><?php echo $data->nama_ruang;?></b> <span class="right"><?php echo $data->tanggal_pinjam;?></span></a>
        <?php
    }
    ?>
</div>

<script>
    $('.alert_close').click(function(){
        $( ".alert_box" ).fadeOut( "slow", function() {
        });
    });
</script>