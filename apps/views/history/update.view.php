<div class="container">
    <div class="row">
        <div class="col s12 m5 card z-depth-3">
            <!-- Start Alert box -->
            <?php
            if (isset($error)) {
                foreach ($error as $error) {
                    ?>
                    <div class="row alert_box">
                        <div class="col s12 m12">
                            <div class="card red darken-2">
                                <div class="row">
                                    <div class="col s9">
                                        <div class="card-content white-text">
                                            <p><?php echo $error; ?></p>
                                        </div>
                                    </div>
                                    <div class="col s3 white-text">
                                        <i class="mdi mdi-close close right alert_close" aria-hidden="true"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php

                }
            }
            ?>
            <!-- End Alert box -->
            <form class="col s12" action="" method="post">
                <div class="row">
                    <div class="input-field col s12">
                        <input type="hidden" name="id_pinjaman" id="id_pinjaman" readonly value="<?php echo $detailPinjam->id_pinjaman;?>" class="validate">
                    </div>
                </div>

                <div class="row">
                    <div class="input-field col s12">
                        <input type="text" name="code_user" id="code_user" readonly value="<?php echo $detailPinjam->code_user;?>" class="validate">
                        <label for="code_user">NIDN/NIK/NPM</label>
                    </div>
                </div>

                <div class="row">
                    <div class="input-field col s12">
                        <input type="text" name="fullname" value="<?php echo $detailPinjam->fullname;?>" readonly class="validate">
                        <label for="fullname">Nama Lengkap</label>
                    </div>
                </div>

                <div class="row">
                    <div class="input-field col s12">
                        <input type="text" id="nama_instansi" name="nama_instansi" value="<?php echo $detailPinjam->nama_instansi;?>" readonly class="validate">
                        <label for="nama_instansi">Nama Instansi</label>
                    </div>
                </div>

                <div class="row">
                    <div class="input-field col s12">
                        <?php
                        $id_hari = $connect->execute("SELECT * FROM tbl_hari WHERE id_hari ORDER BY id_hari ASC");
                        $row_count = $id_hari->num_rows;
                        ?>
                        <select name="id_hari" class="id_hari">
                            <option value="<?php echo $detailPinjam->id_hari;?>" disabled="disabled" selected><?php echo $detailPinjam->nama_hari;?></option>
                            <?php
                            if ($row_count > 0) {
                                while ($row = $id_hari->fetch_object()) {
                                    echo '<option value="' . $row->id_hari . '">' . $row->nama_hari . '</option>';
                                }
                            } else {
                                echo '<option value="">Nama hari tidak tersedia</option>';
                            }
                            ?>
                        </select>
                        <label>Pilih Hari</label>
                    </div>
                </div>

                <div class="row">
                    <div class="input-field col s12">
                        <input type="date" class="datepicker" name="tanggal_pinjam" value="<?php echo $detailPinjam->tanggal_pinjam;?>" id="tanggal_pinjam">
                        <label for="tanggal_pinjam">Tanggal Pinjam</label>
                    </div>
                </div>

                <div class="row">
                    <div class="input-field col s12">
                        <input type="text" class="timepicker" name="jam_awal" value="<?php echo $detailPinjam->jam_awal;?>" id="jam_awal">
                        <label for="jam_awal">Jam Awal</label>
                    </div>
                </div>

                <div class="row">
                    <div class="input-field col s12">
                        <input type="text" class="timepicker" name="jam_akhir" value="<?php echo $detailPinjam->jam_akhir;?>" id="jam_akhir">
                        <label for="jam_akhir">Jam Akhir</label>
                    </div>
                </div>

                <div class="row">
                    <div class="input-field col s12">
                        <?php
                        $id_ruang = $connect->execute("SELECT * FROM tbl_ruang WHERE id_ruang ORDER BY id_ruang ASC");
                        $row_count = $id_ruang->num_rows;
                        ?>
                        <select name="id_ruang" class="id_ruang">
                            <option value="<?php echo $detailPinjam->id_ruang;?>" disabled="disabled" selected><?php echo $detailPinjam->nama_ruang;?></option>
                            <?php
                            if ($row_count > 0) {
                                while ($row = $id_ruang->fetch_object()) {
                                    echo '<option value="' . $row->id_ruang . '">' . $row->nama_ruang . '</option>';
                                }
                            } else {
                                echo '<option value="">Nama ruang tidak tersedia</option>';
                            }
                            ?>
                        </select>
                        <label>Pilih Ruang</label>
                    </div>
                </div>

                <div class="row">
                    <div class="input-field col s12">
                        <?php
                        $id_acara = $connect->execute("SELECT * FROM tbl_acara WHERE id_acara ORDER BY id_acara ASC");
                        $row_count = $id_acara->num_rows;
                        ?>
                        <select name="id_acara" class="id_acara">
                            <option value="<?php echo $detailPinjam->id_acara;?>" disabled="disabled" selected><?php echo $detailPinjam->nama_acara;?></option>
                            <?php
                            if ($row_count > 0) {
                                while ($row = $id_acara->fetch_object()) {
                                    echo '<option value="' . $row->id_acara . '">' . $row->nama_acara . '</option>';
                                }
                            } else {
                                echo '<option value="">Nama Acara tidak tersedia</option>';
                            }
                            ?>
                        </select>
                        <label>Pilih Acara</label>
                    </div>
                </div>

                <div class="row">
                    <div class="input-field col s12">
                        <textarea id="keterangan" name="keterangan" class="materialize-textarea textarea"><?php echo $detailPinjam->keterangan;?></textarea>
                        <label for="keterangan">Keterangan</label>
                    </div>
                </div>
                <div class="row">
                    <div class="col s12">
                        <button  type="submit" name="btn_update_of_borrow" class="waves-effect waves-light btn col s12">Ubah Peminjaman</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript">
    (function($){
        $(function(){

            $('select').material_select();
            
            $('.alert_close').click(function(){
                $( ".alert_box" ).fadeOut( "slow", function() {
                });
            });

            $('.datepicker').pickadate({
                selectMonths: true, // Creates a dropdown to control month
                selectYears: 15, // Creates a dropdown of 15 years to control year,
                today: 'Today',
                clear: 'Clear',
                close: 'Ok',
                closeOnSelect: false // Close upon selecting a date,
            });

            $('.timepicker').pickatime({
                default: 'now', // Set default time: 'now', '1:30AM', '16:30'
                fromnow: 0,       // set default time to * milliseconds from now (using with default = 'now')
                twelvehour: false, // Use AM/PM or 24-hour format
                donetext: 'OK', // text for done-button
                cleartext: 'Clear', // text for clear-button
                canceltext: 'Cancel', // Text for cancel-button
                autoclose: false, // automatic close timepicker
                ampmclickable: true, // make AM PM clickable
                aftershow: function(){} //Function for after opening timepicker
            });

            $('.textarea').trigger('autoresize');

        }); // end of document ready
    })(jQuery); // end of jQuery name space

    function onlyNumber(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))

            return false;
        return true;
    }
</script>
