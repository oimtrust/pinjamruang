<?php
/**
 * Created by PhpStorm.
 * User: oimtrust
 * Date: 20/09/17
 * Time: 14:40
 */?>

<div class="container">
    <div class="row alert_box">
        <div class="col s12">
            <div class="card green darken-2">
                <div class="row">
                    <div class="col s9">
                        <div class="card-content white-text">
                            Selamat Datang <?php echo $row->fullname;?>
                        </div>
                    </div>
                    <div class="col s3 white-text">
                        <i class="mdi mdi-close close right alert_close" aria-hidden="true"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div>
        <div class="col s12 m7">
            <div class="card horizontal">
            <div class="card-image">
                <img src="<?php $baseUrl;?>public/img/beautiful-cafe.jpg">
            </div>
            <div class="card-stacked">
                <div class="card-content">
                <p>Anda bisa melakukan peminjaman ruang di sistem kami. Klik tombol Dibawah ini untuk melakukan peminjaman ruang.</p>
                </div>
                <div class="card-action">
                <a href="<?php $baseUrl;?>index.php?page=home&action=borrow">Pinjam Ruang</a>
                </div>
            </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    (function($){
        $(function(){

            //For dialog box
            $('.alert_close').click(function(){
                $( ".alert_box" ).fadeOut( "slow", function() {
                });
            });

        }); // end of document ready
    })(jQuery); // end of jQuery name space
</script>
