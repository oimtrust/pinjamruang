<?php
/**
 * Created by PhpStorm.
 * User: oimtrust
 * Date: 20/09/17
 * Time: 14:40
 */?>
<div class="navbar-fixed z-depth-1">
    <nav class="orange darken-1" role="navigation">
        <div class="nav-wrapper container">
            <a href="#" data-activates="nav-mobile" class="button-collapse show-on-large"><i class="mdi mdi-menu"></i></a>
            <a href="#" class="dropdown-button right" data-activates="menu-right"><i class="right mdi mdi-dots-vertical"></i></a>
            <ul id='menu-right' class='dropdown-content'>
                <li><a href="<?php $baseUrl;?>index.php?page=home&action=profile">Profil</a></li>
                <li><a href="<?php $baseUrl;?>index.php?page=auth&action=logout">Keluar</a></li>
            </ul>
        </div>
    </nav>
</div>

<ul id="nav-mobile" class="side-nav">
    <li><div class="user-view">
            <div class="background">
                <img class="responsive-img" src="<?php $baseUrl;?>public/img/laptop.jpg">
            </div>
            <a href="<?php $baseUrl;?>index.php"><img class="circle" src="<?php $baseUrl;?>public/img/logo.png"></a>
            <span class="card-title  white-text text-darken-4">
                Peminjaman Ruang
            </span>
        </div>
    </li>
    <li><a href="<?php $baseUrl;?>index.php" class="waves-effect"><i class="mdi mdi-view-dashboard small"></i>Dashboard</a></li>
    <li><div class="divider"></div></li>
    <li><a href="<?php $baseUrl;?>index.php?page=home&action=borrow" class="waves-effect"><i class="mdi mdi-sofa small"></i>Pinjam Ruang</a></li>
    <li><a href="<?php $baseUrl;?>index.php?page=home&action=room" class="waves-effect"><i class="mdi mdi-home-modern small"></i>Lihat Ruang</a></li>
    <li><div class="divider"></div></li>
    <li><a href="<?php $baseUrl;?>index.php?page=home&action=history" class="waves-effect"><i class="mdi mdi-buffer small"></i>Daftar Riwayat</a> </li>

</ul>

<script type="text/javascript">
    (function($){
        $(function(){



        }); // end of document ready
    })(jQuery); // end of jQuery name space
</script>
