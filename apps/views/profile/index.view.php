<div class="container">
    <div class="row">
        <?php
        if (isset($_GET['updated'])) {
        ?>
        <div class="row alert_box">
            <div class="col s12">
                <div class="card green darken-2">
                    <div class="row">
                        <div class="col s9">
                            <div class="card-content white-text">
                                Profil berhasil diubah.
                            </div>
                        </div>
                        <div class="col s3 white-text">
                            <i class="mdi mdi-close close right alert_close" aria-hidden="true"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php
            }
        ?>
    <table>
        <tbody>
            <tr>
                <th>NIDN/NIK/NIM</th>
                <td><?php echo $profile->code_user;?></td>
            </tr>
            <tr>
                <th>Username</th>
                <td><?php echo $profile->username;?></td>
            </tr>
            <tr>
                <th>Nama Lengkap</th>
                <td><?php echo $profile->fullname;?></td>
            </tr>
            <tr>
                <th>No. Telepon</th>
                <td><?php echo $profile->phone;?></td>
            </tr>
            <tr>
                <th>Email</th>
                <td><?php echo $profile->email;?></td>
            </tr>
            <tr>
                <th>Status</th>
                <td><?php echo $profile->status;?></td>
            </tr>
            <tr>
                <th>Instansi</th>
                <td><?php echo $profile->nama_instansi;?></td>
            </tr>
            <tr>
                <th>Alamat</th>
                <td><?php echo $profile->address;?></td>
            </tr>
        </tbody>
      </table>
        <a href="<?php $baseUrl;?>index.php?page=home&action=update-password&update_id=<?php echo $profile->id_peminjam;?>" class="waves-effect waves-light btn rounded red darken-1">Ubah Password</a>
        <a href="<?php $baseUrl;?>index.php?page=home&action=profile-update&update_id=<?php echo $profile->id_peminjam;?>" class="waves-effect waves-light btn rounded blue darken-1">Ubah Profil</a>
    </div>
</div>

<script type="text/javascript">
    $('.alert_close').click(function(){
        $( ".alert_box" ).fadeOut( "slow", function() {
        });
    });
</script>