<div class="row">
    <div class="col s12 z-depth-6 card-panel">
        <!-- Start Alert box -->
        <?php
        if (isset($error)) {
            foreach ($error as $error) {
                ?>
                <div class="row alert_box">
                    <div class="col s12 m12">
                        <div class="card red darken-2">
                            <div class="row">
                                <div class="col s9">
                                    <div class="card-content white-text">
                                        <p><?php echo $error; ?></p>
                                    </div>
                                </div>
                                <div class="col s3 white-text">
                                    <i class="mdi mdi-close close right alert_close" aria-hidden="true"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php

            }
        }
        ?>
        <!-- End Alert box -->
        <form method="post" name="form_updated_password">
            <input type="hidden" name="id_peminjam" value="<?php echo $detailPinjam->id_peminjam;?>">
            <div class="row">

                <div class="input-field col s12">
                    <i class="mdi mdi-lock prefix"></i>
                    <input id="old_password" name="old_password" type="password" class="validate" autofocus>
                    <label for="old_password">Password Lama</label>
                </div>

                <div class="input-field col s12">
                    <i class="mdi mdi-lock-reset prefix"></i>
                    <input id="new_password" name="new_password" type="password" class="validate">
                    <label for="new_password">Password Baru</label>
                </div>

                <div class="input-field col s12">
                    <i class="mdi mdi-lock-outline prefix"></i>
                    <input id="confirm_password" name="confirm_password" type="password" class="validate">
                    <label for="confirm_password">Konfirmasi Password</label>
                </div>

            </div>
            <div class="row">
                <button type="submit" name="btn_update" class="btn rounded waves-effect waves-light  darken-4 lighten-1 col s12"><i class="mdi mdi-pencil-lock small"></i> Ubah</button>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">
    $('.alert_close').click(function(){
        $( ".alert_box" ).fadeOut( "slow", function() {
        });
    });
</script>