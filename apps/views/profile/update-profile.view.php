<div class="row">
    <div class="col s12 z-depth-6 card-panel">
        <!-- Start Alert box -->
        <?php
        if (isset($error)) {
            foreach ($error as $error) {
                ?>
                <div class="row alert_box">
                    <div class="col s12 m12">
                        <div class="card red darken-2">
                            <div class="row">
                                <div class="col s9">
                                    <div class="card-content white-text">
                                        <p><?php echo $error; ?></p>
                                    </div>
                                </div>
                                <div class="col s3 white-text">
                                    <i class="mdi mdi-close close right alert_close" aria-hidden="true"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php

            }
        }
        ?>
        <!-- End Alert box -->
        <form method="post">
            <input type="hidden" name="id_peminjam" value="<?php echo $detailPinjam->id_peminjam;?>">
            <div class="row">
                <div class="input-field col s12">
                    <i class="mdi mdi-account-circle prefix"></i>
                    <input id="code_user" name="code_user" type="text" value="<?php echo $detailPinjam->code_user;?>" readonly class="validate" autofocus>
                    <label for="code_user">NIM/NIK/NIDN</label>
                </div>

                <div class="input-field col s12">
                    <i class="mdi mdi-account-circle prefix"></i>
                    <input id="username" name="username" type="text" value="<?php echo $detailPinjam->username;?>" class="validate" autofocus>
                    <label for="username">Username</label>
                </div>

                <div class="input-field col s12">
                    <i class="mdi mdi-face prefix"></i>
                    <input id="fullname" name="fullname" type="text" value="<?php echo $detailPinjam->fullname;?>" class="validate" >
                    <label for="fullname">Nama Lengkap</label>
                </div>

                <div class="input-field col s12">
                    <i class="mdi mdi-cellphone-android prefix"></i>
                    <input id="phone" name="phone" type="text" value="<?php echo $detailPinjam->phone;?>" onkeypress="return onlyNumber(event)" class="validate" >
                    <label for="phone">No. Telepon/HP</label>
                </div>

                <div class="input-field col s12">
                    <i class="mdi mdi-email-outline prefix"></i>
                    <input id="email" name="email" value="<?php echo $detailPinjam->email;?>" type="text" class="validate" >
                    <label for="email">Email</label>
                </div>

                <div class="input-field col s12">
                    <i class="mdi mdi-home-outline prefix"></i>
                    <textarea id="address" name="address" class="materialize-textarea textarea"><?php echo $detailPinjam->address;?></textarea>
                    <label for="address">Alamat</label>
                </div>

                <div class="input-field col s12">
                    <i class="mdi mdi-account-settings-variant prefix"></i>
                    <select name="status">
                        <option value="<?php echo $detailPinjam->status;?>" disabled selected><?php echo $detailPinjam->status;?></option>
                        <option value="Karyawan">Karyawan</option>
                        <option value="Dosen">Dosen</option>
                        <option value="Mahasiswa">Mahasiswa</option>
                    </select>
                    <label>Status</label>
                </div>

                <div class="input-field col s12">
                    <i class="mdi mdi-account-multiple-outline prefix"></i>
                    <?php
                    $id_instansi = $connect->execute("SELECT * FROM tbl_instansi WHERE id_instansi ORDER BY id_instansi ASC");
                    $row_count = $id_instansi->num_rows;
                    ?>
                    <select name="id_instansi" class="id_instansi">
                        <option value="<?php echo $detailPinjam->id_instansi;?>" disabled="disabled" selected><?php echo $detailPinjam->nama_instansi;?></option>
                        <?php
                        if ($row_count > 0) {
                            while ($row = $id_instansi->fetch_object()) {
                                echo '<option value="' . $row->id_instansi . '">' . $row->nama_instansi . '</option>';
                            }
                        } else {
                            echo '<option value="">Nama instansi tidak tersedia</option>';
                        }
                        ?>
                    </select>
                    <label>Pilih Instansi</label>
                </div>
            </div>
            <div class="row">
                <button type="submit" name="btn_update" class="btn rounded waves-effect waves-light  darken-4 lighten-1 col s12"><i class="mdi mdi-account-edit small"></i> Ubah</button>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">
    $('.alert_close').click(function(){
        $( ".alert_box" ).fadeOut( "slow", function() {
        });
    });

    $('.textarea').trigger('autoresize');

    $('select').material_select();

    function onlyNumber(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57))

            return false;
        return true;
    }
</script>