<?php
if (defined('RESTRICTED')) {
} else {
    exit('No direct script access allowed!');
}

if(!isset($_SESSION))
{
    session_start();
}

//if logged in
if (isset($_SESSION['username'])) {
    $connect->redirect($baseUrl . "index.php");
    exit;
}

if (isset($_POST['btn_login'])) {
    $username   = strip_tags($_POST['username']);
    $password   = md5($_POST['password']);

    if ($username == '') {
        $error[]    = "Username masih kosong";
    }
    elseif ($password == '') {
        $error[]    = "Password masih kosong";
    }
    else {
        $check = $connect->execute("SELECT * FROM tbl_peminjam WHERE username = '{$username}'");

        if ($check->num_rows == 0) {
            $error[]    = "Username tidak terdaftar";
        }
        else {
            $login = $check->fetch_assoc();
            if ($password == $login['password']) {
                $_SESSION['username'] = $login['username'];
                $connect->redirect($baseUrl . 'index.php');
                exit;
            }
            else {
                $error[]    = "Password anda salah";
            }
        }
    }
}

include "apps/views/layouts/header.view.php";
include "apps/views/auth/login.view.php";