<?php
if (defined('RESTRICTED')) {
} else {
    exit('No direct script access allowed!');
}

if (isset($_POST['btn_register'])) {
    $code_user      = $_POST['code_user'];
    $username       = $_POST['username'];
    $password       = md5($_POST['password']);
    $fullname       = strip_tags($_POST['fullname']);
    $phone          = strip_tags($_POST['phone']);
    $email          = $_POST['email'];
    $address        = strip_tags($_POST['address']);
    $status         = $_POST['status'];
    $id_instansi    = $_POST['id_instansi'];

    $codeQuery      = $connect->execute("SELECT COUNT(*) AS code_user
            FROM tbl_pegawai AS pegawai
            LEFT JOIN `tbl_mahasiswa` AS mahasiswa
            ON pegawai.`kode_all` = mahasiswa.`kode_all`
            WHERE pegawai.`id_pegawai` = '{$code_user}' OR mahasiswa.`nim` = '{$code_user}'");
    $checkUser      = $codeQuery->fetch_object();

    if ($checkUser->code_user == 0) {
        $error[]    = "NIM/NIDN/NIK Tidak terdaftar di sistem kepegawaian dan atau mahasiswa";
    }
    elseif ($username == '') {
        $error[]    = "Username masih kosong";
    }
    elseif ($password == '') {
        $error[]    = "Password masih kosong";
    }
    elseif ($fullname == '') {
        $error[]    = "Nama lengkap masih kosong";
    }
    elseif (!preg_match("/^[a-zA-Z .]*$/", $fullname)) {
        $error[]    = "Nama lengkap tidak valid, gelar tidak boleh dimasukkan";
    }
    elseif ($phone == '') {
        $error[]    = "No. Telepon masih kosong";
    }
    elseif (!is_numeric($phone)) {
        $error[]    = "No. telepon tidak valid, hanya angka saja";
    }
    elseif ($email == '') {
        $error[]    = "Email masih kosong!";
    }
    elseif (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $error[]    = "Email tidak valid";
    }
    elseif ($address == '') {
        $error[]    = "Alamat masih kosong!";
    }
    elseif (empty($status)) {
        $error[]    = "Status belum dipilih";
    }
    elseif (empty($id_instansi)) {
        $error[]    = "Instansi belum anda pilih";
    }
    else {
        try {
            if ($auth->register($code_user, $username, $password, $fullname, $phone, $email, $address, $status, $id_instansi)) {
                
            }
            $auth->redirect($baseUrl . 'index.php?page=auth&action=register&registered');
        }
        catch(Exception $e) {
            echo $e->getMessage();
        }
    }
}

include "apps/views/layouts/header.view.php";
include "apps/views/auth/register.view.php";