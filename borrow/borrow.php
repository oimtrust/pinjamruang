<?php
/**
 * Created by PhpStorm.
 * User: oimtrust
 * Date: 21/09/17
 * Time: 8:38
 */
if (defined('RESTRICTED')) {
} else {
    exit('No direct script access allowed!');
}

error_reporting(E_ALL & ~E_NOTICE);

//ini_set('display_errors', 1);
//ini_set('display_startup_errors', 1);
//error_reporting(E_ALL);

$user_login = "";

//if not logged in
if (!isset($_SESSION['username'])) {
    $connect->redirect($baseUrl . "index.php?page=auth&action=login");
    exit;
}

//if logged in
$user_login = "{$_SESSION['username']}";

//to retrive user data
$user = $connect->execute("SELECT *
        FROM `tbl_peminjam` AS peminjam
        LEFT JOIN `tbl_instansi` AS instansi
        ON peminjam.`id_instansi` = instansi.`id_instansi`
        WHERE peminjam.`username` = '{$user_login}'");

$row    = $user->fetch_object();

if (isset($_POST['btn_borrow'])) {
    $id_peminjam            = $_POST['id_peminjam'];
    $id_hari                = $_POST['id_hari'];
    $postTanggalPinjam      = $_POST['tanggal_pinjam'];
    $tanggal_pinjam         = Date("Y-m-d", strtotime($postTanggalPinjam));
    $postJamAwal            = $_POST['jam_awal'];
    $jam_awal               = date("H:i:s", strtotime($postJamAwal));
    $postJamAkhir           = $_POST['jam_akhir'];
    $jam_akhir              = date("H:i:s", strtotime($postJamAkhir));
    $id_ruang               = $_POST['id_ruang'];
    $id_acara               = $_POST['id_acara'];
    $keterangan             = $_POST['keterangan'];

    if (empty($id_hari)) {
        $error[] = "Hari belum dipilih";
    }
    elseif ($tanggal_pinjam == '') {
        $error[] = "Tanggal peminjaman masih kosong";
    }
    elseif ($jam_awal == '') {
        $error[] = "Jam awal masih kosong";
    }
    elseif ($jam_akhir == '') {
        $error[]    = "Jam akhir masih kosong";
    }
    elseif (empty($id_ruang)) {
        $error[]    = "Ruang belum dipilih";
    }
    elseif (empty($id_acara)) {
        $error[] = "Acara masih kosong";
    }
    elseif ($keterangan == '') {
        $error[] = "Keterangan masih kosong";
    } else {
        $schedule = $master->validateSchedule($id_ruang, $id_hari, $tanggal_pinjam, $jam_awal, $jam_akhir);

        try {
            if ($schedule === NULL) {
                $master->createBorrowAccepted($id_peminjam, $id_hari, $tanggal_pinjam, $jam_awal, $jam_akhir, $id_ruang, $id_acara, $keterangan);
                $master->setRoomToUse($id_ruang);
                $master->redirect($baseUrl.'index.php?page=home&action=borrow&accepted');
            }
            else {
                $master->createBorrowDenied($id_peminjam, $id_hari, $tanggal_pinjam, $jam_awal, $jam_akhir, $id_ruang, $id_acara, $keterangan);
                $master->redirect($baseUrl.'index.php?page=home&action=borrow&denied');
            }
        } catch (Exception $exception) {
            echo $exception->getMessage();
        }
    }
    
}

include "apps/views/layouts/header.view.php";
include "apps/views/layouts/menu.view.php";
include "apps/views/borrow/index.view.php";
include "apps/views/layouts/footer.view.php";