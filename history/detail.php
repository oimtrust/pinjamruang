<?php
if (defined('RESTRICTED')) {
} else {
    exit('No direct script access allowed!');
}

$user_login = "";

//if not logged in
if (!isset($_SESSION['username'])) {
    $connect->redirect($baseUrl . "index.php?page=auth&action=login");
    exit;
}

//if logged in
$user_login = "{$_SESSION['username']}";

//to retrive user data
$user = $connect->execute("SELECT * FROM tbl_peminjam WHERE username = '{$user_login}'");

if (isset($_GET['detail_id']) && !empty($_GET['detail_id'])) {
    $id_pinjam = $_GET['detail_id'];
    $stmt = $connect->execute("SELECT
						pinjam.id_pinjaman,
						pinjam.id_peminjam,
                        pinjam.id_ruang,
                        ruang.nama_ruang,
                        pinjam.tanggal_pinjam,
                        pinjam.jam_awal,
                        pinjam.jam_akhir,
                        pinjam.id_acara,
                        acara.nama_acara,
                        pinjam.keterangan,
						pinjam.status,
                        pinjam.alasan,
						pinjam.updated_at,
						peminjam.fullname,
						peminjam.id_instansi,
						instansi.nama_instansi
					FROM
						tbl_pinjaman AS pinjam
                        LEFT JOIN tbl_peminjam AS peminjam ON pinjam.id_peminjam = peminjam.id_peminjam
                        LEFT JOIN tbl_instansi AS instansi ON peminjam.id_instansi = instansi.id_instansi
                        LEFT JOIN tbl_ruang AS ruang ON pinjam.id_ruang = ruang.id_ruang
                        LEFT JOIN tbl_acara AS acara ON pinjam.id_acara = acara.id_acara
                    WHERE pinjam.id_pinjaman = '{$id_pinjam}'");
    $detailPinjam = $stmt->fetch_object();
} else {
    $connect->redirect($baseUrl . 'index.php?page=home&action=history');
}

include "apps/views/layouts/header.view.php";
include "apps/views/layouts/menu.view.php";
include "apps/views/history/detail.view.php";
include "apps/views/layouts/footer.view.php";
