<?php
if (defined('RESTRICTED')) {
} else {
    exit('No direct script access allowed!');
}
error_reporting(E_ALL & ~E_NOTICE);

$user_login = "";

//if not logged in
if (!isset($_SESSION['username'])) {
    $connect->redirect($baseUrl . "index.php?page=auth&action=login");
    exit;
}

//if logged in
$user_login = "{$_SESSION['username']}";

//to retrive user data
$user = $connect->execute("SELECT * FROM tbl_peminjam WHERE username = '{$user_login}'");

if (isset($_GET['update_id']) && !empty($_GET['update_id'])) {
    $id_pinjam = $_GET['update_id'];
    $stmt = $connect->execute("SELECT
						pinjam.id_pinjaman,
						pinjam.id_peminjam,
                        pinjam.id_ruang,
                        ruang.nama_ruang,
                        pinjam.id_hari,
                        hari.nama_hari,
                        pinjam.tanggal_pinjam,
                        pinjam.jam_awal,
                        pinjam.jam_akhir,
                        pinjam.id_acara,
                        acara.nama_acara,
                        pinjam.keterangan,
						pinjam.status,
                        pinjam.alasan,
						pinjam.updated_at,
						peminjam.fullname,
						peminjam.id_instansi,
						peminjam.code_user,
						instansi.nama_instansi
					FROM
						tbl_pinjaman AS pinjam
                        LEFT JOIN tbl_peminjam AS peminjam ON pinjam.id_peminjam = peminjam.id_peminjam
                        LEFT JOIN tbl_hari AS hari ON pinjam.id_hari = hari.id_hari
                        LEFT JOIN tbl_instansi AS instansi ON peminjam.id_instansi = instansi.id_instansi
                        LEFT JOIN tbl_ruang AS ruang ON pinjam.id_ruang = ruang.id_ruang
                        LEFT JOIN tbl_acara AS acara ON pinjam.id_acara = acara.id_acara
                    WHERE pinjam.id_pinjaman = '{$id_pinjam}'");
    $detailPinjam = $stmt->fetch_object();
} else {
    $connect->redirect($baseUrl . 'index.php?page=home&action=history');
}

if (isset($_POST['btn_update_of_borrow'])) {
    $id_pinjaman            = $_POST['id_pinjaman'];
    $id_hari                = $_POST['id_hari'];
    $postTanggalPinjam      = $_POST['tanggal_pinjam'];
    $tanggal_pinjam         = Date("Y-m-d", strtotime($postTanggalPinjam));
    $postJamAwal            = $_POST['jam_awal'];
    $jam_awal               = date("H:i:s", strtotime($postJamAwal));
    $postJamAkhir           = $_POST['jam_akhir'];
    $jam_akhir              = date("H:i:s", strtotime($postJamAkhir));
    $id_ruang               = $_POST['id_ruang'];
    $id_acara               = $_POST['id_acara'];
    $keterangan             = $_POST['keterangan'];

    if (empty($id_hari)) {
        $error[] = "Hari belum dipilih";
    }
    elseif ($tanggal_pinjam == '') {
        $error[] = "Tanggal peminjaman masih kosong";
    }
    elseif ($jam_awal == '') {
        $error[] = "Jam awal masih kosong";
    }
    elseif ($jam_akhir == '') {
        $error[]    = "Jam akhir masih kosong";
    }
    elseif (empty($id_ruang)) {
        $error[]    = "Ruang belum dipilih";
    }
    elseif (empty($id_acara)) {
        $error[] = "Acara masih kosong";
    }
    elseif ($keterangan == '') {
        $error[] = "Keterangan masih kosong";
    }
    else {
        $schedule = $master->validateSchedule($id_ruang, $id_hari, $tanggal_pinjam, $jam_awal, $jam_akhir);

        try {
            if ($schedule === NULL) {
                $master->updateBorrowAccepted($id_hari, $tanggal_pinjam, $jam_awal, $jam_akhir, $id_ruang, $id_acara, $keterangan, $id_pinjaman);
                $master->setRoomToUse($id_ruang);
                $master->redirect($baseUrl.'index.php?page=home&action=history&accepted');
            }
            else {
                $master->updateBorrowDenied($id_hari, $tanggal_pinjam, $jam_awal, $jam_akhir, $id_ruang, $id_acara, $keterangan, $id_pinjaman);
                $master->redirect($baseUrl.'index.php?page=home&action=history&denied');
            }
        } catch (Exception $exception) {
            echo $exception->getMessage();
        }
    }
}

include "apps/views/layouts/header.view.php";
include "apps/views/layouts/menu.view.php";
include "apps/views/history/update.view.php";
include "apps/views/layouts/footer.view.php";
