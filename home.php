<?php
/**
 * Created by PhpStorm.
 * User: oimtrust
 * Date: 20/09/17
 * Time: 14:31
 */

if (defined('RESTRICTED')) {
} else {
    exit('No direct script access allowed!');
}

$user_login = "";

//if not logged in
if (!isset($_SESSION['username'])) {
    $connect->redirect($baseUrl . "index.php?page=auth&action=login");
    exit;
}

//if logged in
$user_login = "{$_SESSION['username']}";

//to retrive user data
$user = $connect->execute("SELECT * FROM tbl_peminjam WHERE username = '{$user_login}'");

$row = $user->fetch_object();

include "apps/views/layouts/header.view.php";
include "apps/views/layouts/menu.view.php";
include "apps/views/index.view.php";
include "apps/views/layouts/footer.view.php";