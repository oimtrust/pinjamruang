<?php
/**
 * Created by PhpStorm.
 * User: oimtrust
 * Date: 20/09/17
 * Time: 14:31
 */
define('RESTRICTED', 1);

//Ensure that a session exists (just in case)
if (!session_id())
{
    session_start();
}

require 'apps/config/app.php';
require_once 'apps/model/Connection.php';
require_once 'apps/model/Authentification.php';
require_once 'apps/model/Mastering.php';

//here our routes
$page   = (!empty($_GET['page'])) ? $_GET['page'] : null;
$action = (!empty($_GET['action'])) ? $_GET['action'] : null;

$connect    = new Connection();
$auth       = new Authentification();
$master     = new Mastering();

switch ($page){
    case 'home':
        if ($action == 'borrow'){
            require 'borrow/borrow.php';
        }
        elseif ($action == 'history') {
            require 'history/history.php';
        }
        elseif ($action == 'detail') {
            require 'history/detail.php';
        }
        elseif ($action == 'update') {
            require 'history/update.php';
        }
        elseif ($action == 'delete') {
            require 'history/delete.php';
        }
        elseif ($action == 'room') {
            require 'room/room.php';
        }
        elseif ($action == 'profile') {
            require 'profile/profile.php';
        }
        elseif ($action == 'profile-update') {
            require 'profile/update-profile.php';
        }
        elseif ($action == 'update-password') {
            require 'profile/update-password.php';
        }
        else {
            require 'error/404.php';
        }
        break;
    
    case 'auth':
        if ($action == 'login') {
            require 'auth/login.php';
        }
        elseif ($action == 'register') {
            require 'auth/register.php';
        }
        elseif ($action == 'logout') {
            require 'auth/logout.php';
        }
        else {
            require 'error/404.php';
        }
        break;

    default:
        require 'home.php';
        break;
}
