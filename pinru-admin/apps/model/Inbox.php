<?php

include_once 'Connection.php';

class Inbox extends Connection
{
    public function setAccept($status, $id_pinjam)
    {
        $result = $this->db->query("UPDATE tbl_pinjaman SET status = 'DITERIMA' WHERE id_pinjam='{$id_pinjam}'");
    }

    public function setReject($status, $id_pinjam)
    {
        $result = $this->db->query("UPDATE tbl_pinjaman SET status = 'DITOLAK' WHERE id_pinjam = '{$id_pinjam}'");
    }

    public function setUse($status, $id_ruang)
    {
        $result = $this->db->query("UPDATE tbl_ruang SET status = 'TERPAKAI' WHERE id_ruang = '{$id_ruang}'");
    }

    public function sendReason($alasan, $id_pinjam)
    {
        $result = $this->db->query("UPDATE tbl_pinjaman SET alasan = '{$alasan}' WHERE id_pinjam = '{$id_pinjam}'");
    }
}