<?php
include_once 'Connection.php';

class Mastering extends Connection
{
    public function createEdifice($nama_gedung, $created_at, $updated_at)
    {
        $result = $this->db->query("INSERT INTO tbl_gedung (nama_gedung, created_at, updated_at)
        VALUES ('{$nama_gedung}', '{$created_at}', '{$updated_at}')");
    }

    public function updateEdifice($nama_gedung, $updated_at, $id_gedung)
    {
        $result = $this->db->query("UPDATE tbl_gedung SET nama_gedung = '{$nama_gedung}', updated_at = '{$updated_at}'
        WHERE id_gedung = '{$id_gedung}'");
    }

    public function deleteEdifice($id_gedung)
    {
        $result = $this->db->query("DELETE FROM tbl_gedung WHERE id_gedung = '{$id_gedung}'");
    }

    public function createRoom($nama_ruang, $id_gedung, $created_at, $updated_at)
    {
        $result = $this->db->query("INSERT INTO tbl_ruang(nama_ruang, id_gedung, created_at, updated_at)
        VALUES('{$nama_ruang}', '{$id_gedung}', '{$created_at}', '{$updated_at}')");
    }

    public function updateRoom($nama_ruang, $id_gedung, $updated_at, $id_ruang)
    {
        $result = $this->db->query("UPDATE tbl_ruang SET nama_ruang = '{$nama_ruang}', id_gedung = '{$id_gedung}',
        updated_at = '{$updated_at}' WHERE id_ruang = '{$id_ruang}'");
    }

    public function deleteRoom($id_ruang)
    {
        $result = $this->db->query("DELETE FROM tbl_ruang WHERE id_ruang = '{$id_ruang}'");
    }

    public function createAgency($nama_instansi, $created_at, $updated_at)
    {
        $result = $this->db->query("INSERT INTO tbl_instansi(nama_instansi, created_at, updated_at)
        VALUES('{$nama_instansi}', '{$created_at}', '{$updated_at}')");
    }

    public function updateAgency($nama_instansi, $updated_at, $id_instansi)
    {
        $result = $this->db->query("UPDATE tbl_instansi SET nama_instansi = '{$nama_instansi}',
        updated_at = '{$updated_at}' WHERE id_instansi = '{$id_instansi}'");
    }

    public function deleteAgency($id_instansi)
    {
        $result = $this->db->query("DELETE FROM tbl_instansi WHERE id_instansi = '{$id_instansi}'");
    }

    public function createEvent($nama_acara, $created_at, $updated_at)
    {
        $result = $this->db->query("INSERT INTO tbl_acara(nama_acara, created_at, updated_at)
        VALUES('{$nama_acara}', '{$created_at}', '{$updated_at}')");
    }

    public function updateEvent($nama_acara, $updated_at, $id_acara)
    {
        $result = $this->db->query("UPDATE tbl_acara SET nama_acara = '{$nama_acara}', updated_at = '{$updated_at}' WHERE id_acara = '{$id_acara}'");
    }

    public function deleteEvent($id_acara)
    {
        $result = $this->db->query("DELETE FROM tbl_acara WHERE id_acara = '{$id_acara}'");
    }
}
