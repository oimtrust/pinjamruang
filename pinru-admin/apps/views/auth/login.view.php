<div class="row">
    <div class="col s12 z-depth-6 card-panel">
        <form class="login-form" method="post">
            <div class="row">
                <div class="input-field col s12 center">
                    <a href="#">
                        <img src="<?php $baseUrl;?>public/img/logo.png" alt="" class="responsive-img valign profile-image-login">
                    </a>
                </div>
            </div>
            <div class="row">
                <?php
                if (isset($error)) {
                    foreach ($error as $error) {
                        ?>
                        <div class="row alert_box">
                            <div class="col s12">
                                <div class="card red darken-2">
                                    <div class="row">
                                        <div class="col s9">
                                            <div class="card-content white-text">
                                                <?php echo $error; ?>
                                            </div>
                                        </div>
                                        <div class="col s3 white-text">
                                            <i class="mdi mdi-close close right alert_close" aria-hidden="true"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php
                    }
                }
                ?>
            </div>
            <div class="row">
                <div class="input-field col s12">
                    <i class="mdi mdi-account-circle prefix"></i>
                    <input id="username" name="username" type="text" class="validate" autofocus>
                    <label for="username">Username</label>
                </div>
                <div class="input-field col s12">
                    <i class="mdi mdi-key-variant prefix"></i>
                    <input id="password" name="password" type="password" class="validate">
                    <label for="password">Password</label>
                </div>
            </div>
            <div class="row">
                <button type="submit" name="btn_login" class="btn waves-effect waves-light deep-purple darken-4 lighten-1 col s12"><i class="mdi mdi-login-variant small"></i> Masuk</button>
            </div>
        </form>
    </div>
</div>

<style type="text/css">
    /*
 * Just for auth design CSS
 */

    html,
    body {
        background-color: #ff6f00;
        height: 100%;
    }
    html {
        display: table;
        margin: auto;
    }
    body {
        display: table-cell;
        vertical-align: middle;
    }
    .margin {
        margin: 0 !important;
    }
</style>

<script type="text/javascript">
    $('.alert_close').click(function(){
        $( ".alert_box" ).fadeOut( "slow", function() {
        });
    });
</script>
