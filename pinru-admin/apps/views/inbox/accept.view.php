<div class="container">
	<div class="row">
		<div class="collection">
			<?php
		$limit = 20;
		$pagination = isset($_GET['pagination']) ? $_GET['pagination'] : "";

		if (empty($pagination)) {
			$position = 0;
			$pagination = 1;
		} else {
			$position = ($pagination - 1) * $limit;
		}

		$query = $connect->execute("SELECT
						pinjam.id_pinjam,
						pinjam.id_member,
						pinjam.kode_peminjam,
						pinjam.status,
						pinjam.updated_at,
						peminjam.fullname
					FROM
						tbl_pinjaman AS pinjam LEFT JOIN tbl_peminjam AS peminjam ON pinjam.id_member = peminjam.id_member
					WHERE pinjam.status = 'DITERIMA' ORDER BY updated_at DESC LIMIT $position, $limit");

		while ($data = $query->fetch_object()) {
			?>
				<a href="<?php $baseUrl;?>index.php?page=home&action=detail-after&detail_id=<?php echo $data->id_pinjam;?>" class="collection-item waves-effect waves-light"><?php echo $data->fullname; ?> <span class="right"><?php echo $data->updated_at; ?></span></a>	
				<?php

		}
		?>
      	</div>
	</div>
		<?php
	$amountOfData = $connect->execute("SELECT
						pinjam.id_pinjam,
						pinjam.id_member,
						pinjam.kode_peminjam,
						pinjam.status,
						pinjam.updated_at,
						peminjam.fullname
					FROM
						tbl_pinjaman AS pinjam LEFT JOIN tbl_peminjam AS peminjam ON pinjam.id_member = peminjam.id_member
					WHERE pinjam.status = 'DITERIMA' ORDER BY updated_at DESC");
	$rows = $amountOfData->num_rows;
	$amountOfPage = ceil($rows / $limit);

	if ($pagination > 1) {
		$link = $pagination - 1;
		$prev = "<a href='" . $baseUrl . "index.php?page=home&action=wait&pagination=" . $link . "' class='btn-floating left purple'><i class='mdi mdi-arrow-left-drop-circle-outline'></i></a>";
	} else {
		$prev = "<a href='#' class='btn-floating left purple'><i class='mdi mdi-arrow-left-drop-circle-outline'></i></a>";
	}

	if ($pagination < $amountOfPage) {
		$link = $pagination + 1;
		$next = "<a href='" . $baseUrl . "index.php?page=home&action=wait&pagination=" . $link . "' class='btn-floating right purple'><i class='mdi mdi-arrow-right-drop-circle-outline'></i></a>";
	} else {
		$next = "<a class='btn-floating right purple'><i class='mdi mdi-arrow-right-drop-circle-outline'></i></a>";
	}
	echo "<div class='row'>
				" . $prev . "
				" . $next . "
			</div>";
	?>
</div>