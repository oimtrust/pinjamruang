<div class="container">
    <form method="post">
        <div class="row">
            <div class="col s12">
            <div class="card">
                <div class="card-image">
                <img src="<?php $baseUrl; ?>public/img/office.jpg">
                <span class="card-title"><?php echo $detailPinjam->fullname; ?></span>
                </div>
                <div class="card-content">
                    <h5><?php echo $detailPinjam->nama_acara;?></h5>
                    <p>
                        <?php echo $detailPinjam->keterangan; ?>
                    </p>
                </div>
            </div>

            <div class="card-panel  orange lighten-5">
                <h5 >INSTANSI</h5>
                <span><?php echo $detailPinjam->nama_instansi; ?></span>
            </div>

            <div class="card-panel  orange lighten-4">
                <h5 >RUANG YANG DIPINJAM</h5>
                <span ><?php echo $detailPinjam->nama_ruang; ?></span>
            </div>

            <div class="card-panel  orange lighten-2">
                <h5 >TANGGAL MULAI</h5>
                <span ><?php echo $detailPinjam->tgl_awal; ?></span>
            </div>

            <div class="card-panel orange lighten-1">
                <h5 >TANGGAL AKHIR</h5>
                <span ><?php echo $detailPinjam->tgl_akhir; ?></span>
            </div>

            <div class="card-panel orange">
                <h5 >Alasan</h5>
                <span ><?php echo $detailPinjam->alasan; ?></span>
            </div>
            </div>
        </div>
    </form>
</div>

<script>
    tippy('.tooltip', {
        placement: 'left',
        animation: 'scale',
        theme: 'menu light',
        trigger: 'click',
        duration: 1000,
        arrow: true
    })
</script>