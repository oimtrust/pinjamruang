<div class="row">
    <?php
    if (isset($_GET['saved'])) {
        ?>
        <div class="row alert_box">
            <div class="col s12">
                <div class="card green darken-2">
                    <div class="row">
                        <div class="col s9">
                            <div class="card-content white-text">
                                Selamat! Data berhasil dikirim.
                            </div>
                        </div>
                        <div class="col s3 white-text">
                            <i class="mdi mdi-close close right alert_close" aria-hidden="true"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php

    }
    ?>

    <div class="col s12 m3">
        <div class="card green accent-3">
            <div class="card-content white-text">
                <span class="card-title"><?php echo $rowOfAcceptInbox->total; ?> Pinjaman di Terima</span>
                <p>
                    <i class="mdi mdi-checkbox-multiple-marked-circle-outline large"></i>
                </p>
            </div>
            <div class="card-action">
                <a href="<?php $baseUrl; ?>index.php?page=home&action=accept" class="white-text"><i class="mdi mdi-account-search"></i> Lihat</a>
            </div>
        </div>
    </div>

    <div class="col s12 m3">
        <div class="card red accent-3">
            <div class="card-content white-text">
                <span class="card-title"><?php echo $rowOfRejectInbox->total; ?> Pinjaman di Tolak</span>
                <p>
                    <i class="mdi mdi-alert-circle-outline large"></i>
                </p>
            </div>
            <div class="card-action">
                <a href="<?php $baseUrl; ?>index.php?page=home&action=reject" class="white-text"><i class="mdi mdi-account-search"></i> Lihat</a>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    (function($){
        $(function(){

            //For dialog box
            $('.alert_close').click(function(){
                $( ".alert_box" ).fadeOut( "slow", function() {
                });
            });

        }); // end of document ready
    })(jQuery); // end of jQuery name space
</script>