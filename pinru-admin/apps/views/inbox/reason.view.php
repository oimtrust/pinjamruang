<div class="container">
    <form method="post">
        <?php
            if (isset($error)) {
                foreach ($error as $error) {
                    ?>
                    <div class="row alert_box">
                        <div class="col s12 m12">
                            <div class="card red darken-2">
                                <div class="row">
                                    <div class="col s9">
                                        <div class="card-content white-text">
                                            <p><?php echo $error; ?></p>
                                        </div>
                                    </div>
                                    <div class="col s3 white-text">
                                        <i class="mdi mdi-close close right alert_close" aria-hidden="true"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php

                }
            }?>
        <div class="row">
            <div class="row">
                <input type="hidden" name="id_pinjam" value="<?php echo $data_reason->id_pinjam; ?>">
                <div class="input-field col s12">
                <textarea id="reason" name="alasan" class="materialize-textarea"></textarea>
                <label for="reason">Alasan</label>
                </div>
            </div>

            <div class="row col s12">
                <button type="submit" name="btn_send" class="btn waves-effect waves-light col s12">Kirim</button>
            </div>
        </div>
    </form>
</div>

<script type="text/javascript">
    $('#reason').trigger('autoresize');

    //For dialog box
    $('.alert_close').click(function(){
        $( ".alert_box" ).fadeOut( "slow", function() {
        });
    });

</script>