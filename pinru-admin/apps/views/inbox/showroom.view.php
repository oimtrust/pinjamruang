<div class="container">
    <form method="post">
        <div class="col s12">
            <div class="card-panel teal">
                <h5 class="orange-text">Ruang Yang Diminta</h5>
                <h4 class="white-text"><?php echo $data->nama_ruang;?></h4>
                <span class="yellow-text right"><?php echo $data->status;?></span>
            </div>
        </div>
        <div class="row">
            <div class="col s4 m6">
            
            </div>
            <div class="chip purple">
                <span class="white-text">Rekomendasi :</span>
            </div>
        </div>
        <?php
        $id_building = $data->id_gedung;

        $room_name = $connect->execute("SELECT nama_ruang, `status` FROM tbl_ruang WHERE '{$id_building}'");

        while ($row = $room_name->fetch_object()) {
            if ($row->status == 'KOSONG') {
                $color = "green";
            }
            else {
                $color = "red";
            }
            ?>
            <div class="col s12">
                <div class="card-panel <?php echo $color;?>">
                    <h6 class="white-text"><?php echo $row->nama_ruang;?></h6>
                </div>
            </div>
            <?php
        }
        ?>
    </form>
</div>