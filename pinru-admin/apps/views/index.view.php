<div class="row">
    <div class="col s12 m3">
        <div class="card  blue accent-1">
            <div class="card-content white-text">
                <span class="card-title"> <?php echo $rowOfInbox->total; ?> Pesan Masuk</span>
                <p>
                    <i class="mdi mdi-email-outline large"></i>
                </p>
            </div>
            <div class="card-action">
                <a href="<?php $baseUrl;?>index.php?page=home&action=inbox" class="white-text"><i class="mdi mdi-account-search"></i> Lihat</a>
            </div>
        </div>
    </div>

    <div class="col s12 m3">
        <div class="card purple darken-4">
            <div class="card-content white-text">
                <span class="card-title"> <?php echo $rowOfMember->total; ?> Member</span>
                <p>
                    <i class="mdi mdi-account-multiple-outline large"></i>
                </p>
            </div>
            <div class="card-action">
                <a href="<?php $baseUrl;?>index.php?page=home&action=member" class="white-text"><i class="mdi mdi-account-search"></i> Lihat</a>
            </div>
        </div>
    </div>
</div>
