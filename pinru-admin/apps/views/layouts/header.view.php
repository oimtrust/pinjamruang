<!DOCTYPE html>
<html>
<head>
    <title>Peminjaman Ruang | UNIKAMA</title>

    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

    <!--Import Google Icon Font-->
    <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css"  media="screen,projection"/>

    <!-- Material Design Icon -->
    <link rel="stylesheet" href="//cdn.materialdesignicons.com/2.0.46/css/materialdesignicons.min.css">

    <!-- Tippy CSS -->
    <link rel="stylesheet" type="text/css" href="https://unpkg.com/tippy.js@1.2.0/dist/tippy.css">

    <!-- Sweet Alert CSS-->
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.css">

    <!-- Style CSS-->
<!--    <link rel="stylesheet" type="text/css" href="--><?php //$baseUrl; ?><!--public/css/style.css">-->


    <!--Import jQuery before materialize.js-->
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>

    <!--Jquery ui js-->
    <script type="text/javascript" src="http://code.jquery.com/ui/1.10.1/jquery-ui.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>

    <!-- Tippy JS -->
    <script type="text/javascript" src="https://unpkg.com/tippy.js@1.2.0/dist/tippy.min.js"></script>

    <!-- Sweet Alert JS -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>

    <!-- Chart JS -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js"></script>

    <!-- Custom JS -->
    <script type="text/javascript" src="<?php $baseUrl;?>public/js/init.js"></script>
</head>

<body>
