<div class="navbar-fixed z-depth-1">
    <nav class="orange darken-1" role="navigation">
        <div class="nav-wrapper container">
            <a href="#" data-activates="nav-mobile" class="button-collapse show-on-large"><i class="mdi mdi-menu"></i></a>
            <a href="#" class="dropdown-button right" data-activates="menu-right"><i class="right mdi mdi-dots-vertical"></i></a>
            <ul id='menu-right' class='dropdown-content'>
                <li><a href="<?php $baseUrl;?>index.php?page=home&action=profile">Profil</a></li>
                <li><a href="<?php $baseUrl;?>index.php?page=auth&action=logout">Keluar</a></li>
            </ul>
        </div>
    </nav>
</div>

<ul id="nav-mobile" class="side-nav">
    <li><div class="user-view">
            <div class="background">
                <img class="responsive-img" src="<?php $baseUrl;?>public/img/laptop.jpg">
            </div>
            <a href="<?php $baseUrl;?>index.php"><img class="circle" src="<?php $baseUrl;?>public/img/logo.png"></a>
            <span class="card-title  orange-text text-darken-4">
                Peminjaman Ruang
            </span>
        </div>
    </li>
    <li><a href="<?php $baseUrl;?>index.php" class="waves-effect"><i class="mdi mdi-view-dashboard small orange-text"></i>Dashboard</a></li>
    <li><div class="divider"></div></li>
    <li><a href="<?php $baseUrl;?>index.php?page=home&action=inbox" class="waves-effect"><i class="mdi mdi-email-outline small red-text"></i>Kotak Masuk</a></li>
    <li><a href="<?php $baseUrl;?>index.php?page=home&action=room" class="waves-effect"><i class="mdi mdi-sofa small brown-text"></i>Lihat Ruang</a></li>
    <li><a href="<?php $baseUrl;?>index.php?page=home&action=member" class="waves-effect"><i class="mdi mdi-face-profile small yellow-text"></i>Lihat Member</a></li>
    <li><div class="divider"></div></li>
    <li class="no-padding">
        <ul class="collapsible collapsible-accordion">
          <li>
            <a class="collapsible-header">Mastering<i class="mdi mdi-chevron-down right"></i></a>
            <div class="collapsible-body">
              <ul>
                <li><a href="<?php $baseUrl;?>index.php?page=home&action=edifice">Gedung <i class="mdi mdi-home-modern small blue-text"></i></a></li>
                <li><a href="<?php $baseUrl; ?>index.php?page=home&action=mroom">Ruang<i class="mdi mdi-sofa small green-text"></i></a></li>
                <li><a href="<?php $baseUrl; ?>index.php?page=home&action=agency">Instansi <i class="mdi mdi-account-multiple-outline small purple-text"></i></a></li>
                <li><a href="<?php $baseUrl; ?>index.php?page=home&action=event">Acara <i class="mdi mdi-calendar-check small pink-text"></i></a></li>
              </ul>
            </div>
          </li>
        </ul>
    </li>
    <li><div class="divider"></div></li>
    <li><a href="<?php $baseUrl;?>index.php?page=home&action=report" class="waves-effect"><i class="mdi mdi-chart-line small"></i>Grafik</a> </li>

</ul>

<script type="text/javascript">
    (function($){
        $(function(){

            

        }); // end of document ready
    })(jQuery); // end of jQuery name space
</script>
