<div class="container">
    <?php
    if (isset($error)) {
        foreach ($error as $error) {
            ?>
            <div class="row alert_box">
                <div class="col s12">
                    <div class="card red darken-2">
                        <div class="row">
                            <div class="col s9">
                                <div class="card-content white-text">
                                    <?php echo $error; ?>
                                </div>
                            </div>
                            <div class="col s3 white-text">
                                <i class="mdi mdi-close close right alert_close" aria-hidden="true"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php

        }
    } elseif (isset($_GET['error'])) {
        ?>
            <div class="row alert_box">
                <div class="col s12">
                    <div class="card red darken-2">
                        <div class="row">
                            <div class="col s9">
                                <div class="card-content white-text">
                                    Peringatan! Anda harus memilih data untuk di ubah!
                                </div>
                            </div>
                            <div class="col s3 white-text">
                                <i class="mdi mdi-close close right alert_close" aria-hidden="true"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php

        } elseif (isset($_GET['updated'])) {
            ?>
                <div class="row alert_box">
                    <div class="col s12">
                        <div class="card green darken-2">
                            <div class="row">
                                <div class="col s9">
                                    <div class="card-content white-text">
                                        Selamat! Perubahan data berhasil.
                                    </div>
                                </div>
                                <div class="col s3 white-text">
                                    <i class="mdi mdi-close close right alert_close" aria-hidden="true"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php

            } elseif (isset($_GET['deleted'])) {
                ?>
                <div class="row alert_box">
                    <div class="col s12">
                        <div class="card yellow darken-2">
                            <div class="row">
                                <div class="col s9">
                                    <div class="card-content white-text">
                                        Selamat! Data berhasil dihapus.
                                    </div>
                                </div>
                                <div class="col s3 white-text">
                                    <i class="mdi mdi-close close right alert_close" aria-hidden="true"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php

            }
            ?>
    <div class="row">
        <form method="post">
            <ul class="collapsible" data-collapsible="accordion">
            <?php
            $query = $connect->execute("SELECT * FROM tbl_acara ORDER BY updated_at DESC");
            while ($data = $query->fetch_object()) {
                ?>
                <li>
                <div class="collapsible-header"><?php echo $data->nama_acara;?></div>
                <div class="collapsible-body">
                    <a href="<?php $baseUrl;?>index.php?page=home&action=event-update&update_id=<?php echo $data->id_acara;?>" class="btn-floating btn-small waves-effect waves-light blue"><i class="mdi mdi-pencil"></i></a>
                    <a href="<?php $baseUrl;?>index.php?page=home&action=event-delete&delete_id=<?php echo $data->id_acara;?>" class="btn-floating btn-delete btn-small waves-effect waves-light red"><i class="mdi mdi-delete-empty"></i></a>
                </div>
                <?php
            }
            ?>
            </ul>

            <div class="fixed-action-btn">
                <a href="<?php $baseUrl;?>index.php?page=home&action=event_create" class="btn-floating btn-large green">
                <i class="large mdi mdi-plus"></i>
                </a>
            </div>
        </form>
    </div>
</div>

<script>
    $(document).ready(function(){
        $('.collapsible').collapsible();

        $('.alert_close').click(function(){
            $( ".alert_box" ).fadeOut( "slow", function() {
            });
        });

        $('.btn-delete').on('click',function(){
            var getLink = $(this).attr('href');

            swal({
                title: 'Hapus Acara',
                text: 'Loe Yakin Broo?',
                html: true,
                confirmButtonColor: '#d9534f',
                showCancelButton: true,
            },function(){
                window.location.href = getLink
            });

            return false;
        });
    });
</script>