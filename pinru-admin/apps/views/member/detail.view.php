<div class="container">
    <form method="post">
        <div class="row">
            <div class="col s12">
            <div class="card">
                <div class="card-image">
                <img src="<?php $baseUrl; ?>public/img/cat.jpg">
                <span class="card-title"><b><?php echo $data->fullname; ?></b></span>
                </div>
                <div class="card-content">
                <p>
                    <?php echo $data->address; ?>
                </p>
                </div>
            </div>

            <div class="card-panel  orange lighten-5">
                <h5 >USERNAME</h5>
                <span><?php echo $data->username; ?></span>
            </div>

            <div class="card-panel  orange lighten-4">
                <h5 >NO. HANDPHONE</h5>
                <span ><?php echo $data->phone; ?></span>
            </div>

            <div class="card-panel  orange lighten-3">
                <h5 >EMAIL</h5>
                <span ><?php echo $data->email; ?></span>
            </div>

            <div class="card-panel  orange lighten-2">
                <h5 >STATUS</h5>
                <span ><?php echo $data->status; ?></span>
            </div>
            </div>
        </div>
    </form>
</div>

<script>
    tippy('.tooltip', {
        placement: 'left',
        animation: 'scale',
        theme: 'menu light',
        trigger: 'click',
        duration: 1000,
        arrow: true
    })
</script>