<div class="container">
    <?php
    if (isset($error)) {
        foreach ($error as $error) {
            ?>
            <div class="row alert_box">
                <div class="col s12">
                    <div class="card red darken-2">
                        <div class="row">
                            <div class="col s9">
                                <div class="card-content white-text">
                                    <?php echo $error; ?>
                                </div>
                            </div>
                            <div class="col s3 white-text">
                                <i class="mdi mdi-close close right alert_close" aria-hidden="true"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php
        }
    }?>

    <form method="post">
        <div class="row">
            <div class="right">
                <div class="input-field inline">
                    <input id="search" type="text" name="search" class="validate">
                    <label for="search">Cari Member</label>
                </div>
                <button name="btn_search" type="submit" class="btn btn-floating purple accent-3 waves-light waves-effect tooltipped" data-position="bottom" data-delay="50" data-tooltip="Cari"><i class="mdi mdi-account-search"></i> </button>
            </div>
        </div>

        <div class="collection">
            <?php
            if (isset($_POST['btn_search'])) {
                $nameSearch   = $_POST['search'];

                if ($nameSearch == '') {
                    $error[]    = "Inputkan nama untuk melakukan pencarian!";
                }
                elseif ($nameSearch != '') {
                    $query = $connect->execute("SELECT * FROM tbl_peminjam WHERE fullname LIKE '%{$nameSearch}%'");
                }
                else {
                    $query = $connect->execute("SELECT * FROM tbl_peminjam ORDER BY updated_at DESC");
                }
            }
            else {
                $query = $connect->execute("SELECT * FROM tbl_peminjam ORDER BY updated_at DESC");
            } 
            $checkSearch = $query->num_rows;

            if ($checkSearch < 1) {
                ?>
                <div class="">
                    <div class="col s12">
                        <div class="card-panel orange">
                        <span class="white-text">
                        Data Tidak Ada!
                        </span>
                        </div>
                    </div>
                </div>
                <?php
            }
            else {
                while ($data = $query->fetch_object()) {
                    ?>
                    <a href="<?php $baseUrl;?>index.php?page=home&action=memberDetail&detail_id=<?php echo $data->id_member;?>" class="collection-item"><b><?php echo $data->fullname;?></b></a>
                    <?php
                }
            }
            ?>
        </div>
    </form>
</div>

<script type="text/javascript">
    $('.alert_close').click(function(){
        $( ".alert_box" ).fadeOut( "slow", function() {
        });
    });
</script>
