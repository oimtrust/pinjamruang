<form action="" method="post">
  <div class="row">
    <div class="right">
        <div class="input-field inline">
            <input id="search" type="text" name="search" class="validate">
            <label for="search">Cari Ruang</label>
        </div>
        <button name="btn_search" type="submit" class="btn btn-floating purple accent-3 waves-light waves-effect tooltipped" data-position="bottom" data-delay="50" data-tooltip="Cari"><i class="mdi mdi-account-search"></i> </button>
    </div>
  </div>

  <?php
  if (isset($_POST['btn_search'])) {
    $room = $_POST['search'];

    if ($room == '') {
      ?>
      <div class="row alert_box">
                <div class="col s12">
                    <div class="card red darken-2">
                        <div class="row">
                            <div class="col s9">
                                <div class="card-content white-text">
                                    Harap anda inputkan data yang akan dicari
                                </div>
                            </div>
                            <div class="col s3 white-text">
                                <i class="mdi mdi-close close right alert_close" aria-hidden="true"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
      <?php
    }
    else {
      if ($room != '') {
        $query = $connect->execute("SELECT * FROM tbl_ruang WHERE nama_ruang LIKE '%$room%'");
      }
      else {
        $query = $connect->execute("SELECT * FROM tbl_ruang");
      }
    }
  }
  else {
    $query = $connect->execute("SELECT * FROM tbl_ruang");
  }

  $check_search = $query->num_rows;

  if ($check_search < 1) {
    ?>
    <div class="row">
        <div class="col s12 m5">
          <div class="card-panel yellow">
            <span class="white-text">
            Data Tidak ada
            </span>
          </div>
        </div>
    </div>
    <?php
  }
  else {
    while ($data = $query->fetch_object()) {
      if ($data->status == 'KOSONG') {
        $color = "green";
      }
      else{
        $color = "red";
      }
      ?>
      <div class="row">
          <div class="col s12 m5">
            <div class="card-panel <?php echo $color;?>">
              <span class="white-text">
              <?php echo $data->nama_ruang;?>
              </span>
            </div>
          </div>
      </div>
      <?php
    }
  }
  ?>
</form>

<script type="text/javascript">
  $('.alert_close').click(function(){
        $( ".alert_box" ).fadeOut( "slow", function() {
        });
    });
</script>