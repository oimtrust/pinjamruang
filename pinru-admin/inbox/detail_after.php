<?php 
if (defined('RESTRICTED')) {
} else {
    exit('No direct script access allowed!');
}

/**
 * For showing error 500
 */

// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
// error_reporting(E_ALL);


$admin_login = "";

//if not logged in
if (!isset($_SESSION['username'])) {
    $connect->redirect($baseUrl . "index.php?page=auth&action=login");
    exit;
}

//if logged in
$admin_login = "{$_SESSION['username']}";

//to retrive user data
$admin = $connect->execute("SELECT * FROM tbl_admin WHERE username = '{$admin_login}'");

if (isset($_GET['detail_id']) && !empty($_GET['detail_id'])) {
    $id_pinjam = $_GET['detail_id'];
    $stmt = $connect->execute("SELECT
						pinjam.id_pinjam,
						pinjam.id_member,
                        pinjam.id_instansi,
                        instansi.nama_instansi,
                        pinjam.id_ruang,
                        ruang.nama_ruang,
                        pinjam.tgl_awal,
                        pinjam.tgl_akhir,
                        pinjam.id_acara,
                        acara.nama_acara,
                        pinjam.keterangan,
						pinjam.kode_peminjam,
						pinjam.status,
                        pinjam.alasan,
						pinjam.updated_at,
						peminjam.fullname
					FROM
						tbl_pinjaman AS pinjam
                        LEFT JOIN tbl_peminjam AS peminjam ON pinjam.id_member = peminjam.id_member
                        LEFT JOIN tbl_instansi AS instansi ON pinjam.id_instansi = instansi.id_instansi
                        LEFT JOIN tbl_ruang AS ruang ON pinjam.id_ruang = ruang.id_ruang
                        LEFT JOIN tbl_acara AS acara ON acara.id_acara = pinjam.id_acara
                    WHERE pinjam.id_pinjam = '{$id_pinjam}' ORDER BY pinjam.updated_at DESC");
    $detailPinjam = $stmt->fetch_object();
} else {
    $connect->redirect($baseUrl . 'index.php?page=home&action=inbox');
}

include 'apps/views/layouts/header.view.php';
include 'apps/views/layouts/menu.view.php';
include 'apps/views/inbox/detail_after.view.php';
include 'apps/views/layouts/footer.view.php';