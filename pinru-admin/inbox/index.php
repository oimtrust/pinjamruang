<?php 
if (defined('RESTRICTED')) {
} else {
    exit('No direct script access allowed!');
}

/**
 * For showing error 500
 */

// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
// error_reporting(E_ALL);


$admin_login = "";

//if not logged in
if (!isset($_SESSION['username'])) {
    $connect->redirect($baseUrl . "index.php?page=auth&action=login");
    exit;
}

//if logged in
$admin_login = "{$_SESSION['username']}";

//to retrive user data
$admin = $connect->execute("SELECT * FROM tbl_admin WHERE username = '{$admin_login}'");

/**
 * counting accepted inbox
 */
$acceptInbox = $connect->execute("SELECT COUNT(*) AS total FROM tbl_pinjaman WHERE status = 'DITERIMA'");
$rowOfAcceptInbox = $acceptInbox->fetch_object();

/**
 * counting rejected inbox
 */
$rejectInbox = $connect->execute("SELECT COUNT(*) AS total FROM tbl_pinjaman WHERE status = 'DITOLAK'");
$rowOfRejectInbox = $rejectInbox->fetch_object();

include 'apps/views/layouts/header.view.php';
include 'apps/views/layouts/menu.view.php';
include 'apps/views/inbox/index.view.php';
include 'apps/views/layouts/footer.view.php';