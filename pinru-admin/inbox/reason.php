<?php 
if (defined('RESTRICTED')) {
} else {
    exit('No direct script access allowed!');
}

/**
 * For showing error 500
 */

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);


$admin_login = "";

//if not logged in
if (!isset($_SESSION['username'])) {
    $connect->redirect($baseUrl . "index.php?page=auth&action=login");
    exit;
}

//if logged in
$admin_login = "{$_SESSION['username']}";

//to retrive user data
$admin = $connect->execute("SELECT * FROM tbl_admin WHERE username = '{$admin_login}'");

if (isset($_GET['pinjam_id']) && !empty($_GET['pinjam_id'])) {
    $id_pinjam = $_GET['pinjam_id'];
    $stmt = $connect->execute("SELECT * FROM tbl_pinjaman WHERE id_pinjam = '{$id_pinjam}'");
    $data_reason = $stmt->fetch_object();
} else {
    //$connect->redirect($baseUrl . 'index.php?page=home&action=edifice&error');
}

if (isset($_POST['btn_send'])) {
    $id_pinjam  = $_POST['id_pinjam'];
    $alasan     = $_POST['alasan'];

    if ($alasan == '') {
        $error[]    = "Alasan harus ada";
    }
    else {
        try {
            if ($inbox->sendReason($alasan, $id_pinjam)) {
                
            }
            $inbox->redirect($baseUrl.'index.php?page=home&action=inbox&saved');
        }
        catch(Exception $e) {
            echo $e->getMessage();
        }
    }
}

include 'apps/views/layouts/header.view.php';
include 'apps/views/layouts/menu.view.php';
include 'apps/views/inbox/reason.view.php';
include 'apps/views/layouts/footer.view.php';