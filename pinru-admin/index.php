<?php
define('RESTRICTED', 1);

//Ensure that a session exists (just in case)
if (!session_id())
{
    session_start();
}

require 'apps/config/app.php';
require_once 'apps/model/Connection.php';
require_once 'apps/model/Mastering.php';
require_once 'apps/model/Inbox.php';

$connect    = new Connection();
$mastering  = new Mastering();
$inbox      = new Inbox();

//here our routes
$page   = (!empty($_GET['page'])) ? $_GET['page'] : null;
$action = (!empty($_GET['action'])) ? $_GET['action'] : null;

switch ($page) {
  case 'auth':
    if ($action == 'login') {
      require 'auth/login.php';
    } elseif ($action == 'logout') {
      require 'auth/logout.php';
    }
    else {
      require 'error/404.php';
    }
    break;

  case 'home':
    if ($action == 'profile') {
      require 'profile/profile.php';
    }
    elseif ($action == 'inbox') {
      require 'inbox/index.php';
    }
    elseif ($action == 'wait') {
      require 'inbox/wait.php';
    }
    elseif ($action == 'accept') {
      require 'inbox/accept.php';
    }
    elseif ($action == 'reject') {
      require 'inbox/reject.php';
    }
    elseif ($action == 'detail_inbox') {
      require 'inbox/detail.php';
    }
    elseif ($action == 'detail-after') {
      require 'inbox/detail_after.php';
    }
    elseif ($action == 'showroom') {
      require 'inbox/showroom.php';
    }
    elseif ($action == 'reason') {
      require 'inbox/reason.php';
    }
    elseif ($action == 'room') {
      require 'room/room.php';
    }
    elseif ($action == 'member') {
      require 'member/member.php';
    }
    elseif ($action == 'memberDetail') {
      require 'member/detail.php';
    }
    elseif ($action == 'edifice') {
      require 'mastering/edifice/index.php';
    }
    elseif ($action == 'edifice_create') {
      require 'mastering/edifice/create.php';
    }
    elseif ($action == 'edifice_update') {
      require 'mastering/edifice/update.php';
    } 
    elseif ($action == 'edifice_delete') {
      require 'mastering/edifice/delete.php';
    }
    elseif ($action == 'mroom') {
      require 'mastering/mroom/index.php';
    }
    elseif ($action == 'mroom_create') {
      require 'mastering/mroom/create.php';
    }
    elseif ($action == 'mroom_update') {
      require 'mastering/mroom/update.php';
    }
    elseif ($action == 'mroom_delete') {
      require 'mastering/mroom/delete.php';
    }
    elseif ($action == 'agency') {
      require 'mastering/agency/index.php';
    }
    elseif ($action == 'agency_create') {
      require 'mastering/agency/create.php';
    }
    elseif ($action == 'agency_update') {
      require 'mastering/agency/update.php';
    }
    elseif ($action == 'agency_delete') {
      require 'mastering/agency/delete.php';
    }
    elseif ($action == 'event') {
      require 'mastering/event/index.php';
    }
    elseif ($action == 'event_create') {
      require 'mastering/event/create.php';
    }
    elseif ($action == 'event-update') {
      require 'mastering/event/update.php';
    }
    elseif ($action == 'event-delete') {
      require 'mastering/event/delete.php';
    }
    elseif ($action == 'report') {
      require 'report/index.php';
    }
    elseif ($action == 'cronjob-update-ofdate') {
      require 'update-room-auto.php';
    }
    else {
      require 'error/404.php';
    }
    break;

  default:
    require 'home.php';
    break;
}
