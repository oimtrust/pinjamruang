<?php
if (defined('RESTRICTED')) {
} else {
    exit('No direct script access allowed!');
}

/**
 * For showing error 500
 */

// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
// error_reporting(E_ALL);


$admin_login = "";

//if not logged in
if (!isset($_SESSION['username'])) {
    $connect->redirect($baseUrl . "index.php?page=auth&action=login");
    exit;
}

//if logged in
$admin_login = "{$_SESSION['username']}";

//to retrive user data
$admin = $connect->execute("SELECT * FROM tbl_admin WHERE username = '{$admin_login}'");

if (isset($_POST['btn_save'])) {
    $nama_instansi  = strip_tags($_POST['nama_instansi']);
    $created_at = date('Y-m-d H:i:s');
    $updated_at = date('Y-m-d H:i:s');

    if ($nama_instansi == '') {
        $error[]    = "Nama instansi masih kosong!";
    }
    else {
        try {
            if ($mastering->createAgency($nama_instansi, $created_at, $updated_at)) {
                
            }
            $mastering->redirect($baseUrl . 'index.php?page=home&action=agency_create&saved');
        }
        catch(Exception $e) {
            echo $e->getMessage();
        }
    }
}

include 'apps/views/layouts/header.view.php';
include 'apps/views/layouts/menu.view.php';
include 'apps/views/mastering/agency/create.view.php';
include 'apps/views/layouts/footer.view.php';