<?php
if (defined('RESTRICTED')) {
} else {
    exit('No direct script access allowed!');
}

/**
 * For showing error 500
 */

// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
// error_reporting(E_ALL);


$admin_login = "";

//if not logged in
if (!isset($_SESSION['username'])) {
    $connect->redirect($baseUrl . "index.php?page=auth&action=login");
    exit;
}

//if logged in
$admin_login = "{$_SESSION['username']}";

//to retrive user data
$admin = $connect->execute("SELECT * FROM tbl_admin WHERE username = '{$admin_login}'");


//get edit_id for showing data
if (isset($_GET['edit_id']) && !empty($_GET['edit_id'])) {
    $id = $_GET['edit_id'];
    $stmt = $connect->execute("SELECT * FROM tbl_instansi WHERE id_instansi = '{$id}'");
    $data = $stmt->fetch_object();
} else {
    $connect->redirect($baseUrl . 'index.php?page=home&action=edifice&error');
}

if (isset($_POST['btn_update'])) {
    $id_instansi    = $_POST['id_instansi'];
    $nama_instansi  = strip_tags($_POST['nama_instansi']);
    $updated_at     = date('Y-m-d H:i:s');

    if ($nama_instansi == '') {
        $error[]    = "Nama instansi tidak boleh kosong!";
    }
    else {
        try {
            if ($mastering->updateAgency($nama_instansi, $updated_at, $id_instansi)) {
                
            }
            $mastering->redirect($baseUrl . 'index.php?page=home&action=agency&updated');
        }
        catch(Exception $e) {
            echo $e->getMessage();
        }
    }
}

include 'apps/views/layouts/header.view.php';
include 'apps/views/layouts/menu.view.php';
include 'apps/views/mastering/agency/update.view.php';
include 'apps/views/layouts/footer.view.php';