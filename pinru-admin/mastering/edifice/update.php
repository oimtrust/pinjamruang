<?php
if (defined('RESTRICTED')) {
} else {
    exit('No direct script access allowed!');
}

/**
 * For showing error 500
 */

// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
// error_reporting(E_ALL);


$admin_login = "";

//if not logged in
if (!isset($_SESSION['username'])) {
    $connect->redirect($baseUrl . "index.php?page=auth&action=login");
    exit;
}

//if logged in
$admin_login = "{$_SESSION['username']}";

//to retrive user data
$admin = $connect->execute("SELECT * FROM tbl_admin WHERE username = '{$admin_login}'");

//get edit_id for showing data
if (isset($_GET['edit_id']) && !empty($_GET['edit_id'])) {
    $id     = $_GET['edit_id'];
    $stmt   = $connect->execute("SELECT * FROM tbl_gedung WHERE id_gedung = '{$id}'");
    $data   = $stmt->fetch_object();
}
else {
    $connect->redirect($baseUrl.'index.php?page=home&action=edifice&error');
}

if (isset($_POST['btn_update'])) {
    $id_gedung      = $_POST['id_gedung'];
    $nama_gedung    = strip_tags($_POST['nama_gedung']);
    $updated_at     = date('Y-m-d H:i:s');

    if ($nama_gedung == '') {
        $error[]    = "Nama gedung tidak boleh kosong!";
    }
    elseif (strlen($nama_gedung) >= 31) {
        $error[]    = "Nama gedung tidak boleh lebih dari 30 karakter!";    
    }
    else {
        try {
            if ($mastering->updateEdifice($nama_gedung, $updated_at, $id_gedung)) {
                
            }
            $mastering->redirect($baseUrl . 'index.php?page=home&action=edifice&updated');
        } catch(Exception $e) {
            echo $e->getMessage();
        }
    }
}

include 'apps/views/layouts/header.view.php';
include 'apps/views/layouts/menu.view.php';
include 'apps/views/mastering/edifice/update.view.php';
include 'apps/views/layouts/footer.view.php';