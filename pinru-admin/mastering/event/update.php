<?php
if (defined('RESTRICTED')) {
} else {
    exit('No direct script access allowed!');
}

/**
 * For showing error 500
 */

// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
// error_reporting(E_ALL);


$admin_login = "";

//if not logged in
if (!isset($_SESSION['username'])) {
    $connect->redirect($baseUrl . "index.php?page=auth&action=login");
    exit;
}

//if logged in
$admin_login = "{$_SESSION['username']}";

//to retrive user data
$admin = $connect->execute("SELECT * FROM tbl_admin WHERE username = '{$admin_login}'");

if (isset($_GET['update_id']) && !empty($_GET['update_id'])) {
    $id_acara = $_GET['update_id'];
    $stmt = $connect->execute("SELECT * FROM tbl_acara WHERE id_acara = '{$id_acara}'");
    $data = $stmt->fetch_object();
} else {
    $connect->redirect($baseUrl . 'index.php?page=home&action=event&error');
}

if (isset($_POST['btn_update'])) {
    $id_acara   = $_POST['id_acara'];
    $nama_acara = strip_tags($_POST['nama_acara']);
    $updated_at = date('Y-m-d H:i:s');

    if ($nama_acara == '') {
        $error[] = "Nama Acara masih kosong";
    } else {
        try {
            if ($mastering->updateEvent($nama_acara, $updated_at, $id_acara)) {

            }
            $mastering->redirect($baseUrl . 'index.php?page=home&action=event&updated');
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }
}

include 'apps/views/layouts/header.view.php';
include 'apps/views/layouts/menu.view.php';
include 'apps/views/mastering/event/update.view.php';
include 'apps/views/layouts/footer.view.php';