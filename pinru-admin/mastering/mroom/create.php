<?php
if (defined('RESTRICTED')) {
} else {
    exit('No direct script access allowed!');
}

/**
 * For showing error 500
 */

// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
// error_reporting(E_ALL);


$admin_login = "";

//if not logged in
if (!isset($_SESSION['username'])) {
    $connect->redirect($baseUrl . "index.php?page=auth&action=login");
    exit;
}

//if logged in
$admin_login = "{$_SESSION['username']}";

//to retrive user data
$admin = $connect->execute("SELECT * FROM tbl_admin WHERE username = '{$admin_login}'");

if (isset($_POST['btn_save'])) {
    $id_gedung  = $_POST['id_gedung'];
    $nama_ruang = strip_tags($_POST['nama_ruang']);
    $created_at = date('Y-m-d H:i:s');
    $updated_at = date('Y-m-d H:i:s');

    if (empty($id_gedung)) {
        $error[]    = "Anda harus memilih nama gedung dahulu";
    }
    elseif ($nama_ruang == '') {
        $error[]    = "Nama ruang masih kosong!";
    }
    elseif (strlen($nama_ruang) >= 51) {
        $error[]    = "Nama ruang tidak boleh lebih dari 50 karakter";
    }
    else {
        try {
            if ($mastering->createRoom($nama_ruang, $id_gedung, $created_at, $updated_at)) {
                
            }
            $mastering->redirect($baseUrl . 'index.php?page=home&action=mroom_create&saved');
        } catch(Exception $e) {
            echo $e->getMessage();
        }
    }
}

include 'apps/views/layouts/header.view.php';
include 'apps/views/layouts/menu.view.php';
include 'apps/views/mastering/mroom/create.view.php';
include 'apps/views/layouts/footer.view.php';