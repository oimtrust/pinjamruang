<?php
if (defined('RESTRICTED')) {
} else {
    exit('No direct script access allowed!');
}

/**
 * For showing error 500
 */

// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
// error_reporting(E_ALL);


$admin_login = "";

//if not logged in
if (!isset($_SESSION['username'])) {
    $connect->redirect($baseUrl . "index.php?page=auth&action=login");
    exit;
}

//if logged in
$admin_login = "{$_SESSION['username']}";

//to retrive user data
$admin = $connect->execute("SELECT * FROM tbl_admin WHERE username = '{$admin_login}'");

//get edit_id for showing data
if (isset($_GET['edit_id']) && !empty($_GET['edit_id'])) {
    $id = $_GET['edit_id'];
    $stmt = $connect->execute("SELECT room.id_ruang,
                        room.nama_ruang,
                        edifice.id_gedung,
                        edifice.nama_gedung,
                        room.created_at,
                        room.updated_at FROM tbl_ruang AS room
                        LEFT JOIN tbl_gedung AS edifice ON 
                        room.id_gedung = edifice.id_gedung
                        WHERE room.id_ruang = '{$id}'");
    $data = $stmt->fetch_object();
} else {
    $connect->redirect($baseUrl . 'index.php?page=home&action=mroom&error');
}

if (isset($_POST['btn_update'])) {
    $id_ruang       = $_POST['id_ruang'];
    $nama_ruang     = strip_tags($_POST['nama_ruang']);
    $id_gedung      = $_POST['id_gedung'];
    $updated_at     = date('Y-m-d H:i:s');

    if ($nama_ruang == '') {
        $error[]    = "Nama ruang tidak boleh kosong!";
    }
    elseif (strlen($nama_ruang) >= 51) {
        $error[]    = "Nama ruang tidak boleh lebih dari 50 karakter";
    }
    elseif (empty($id_gedung)) {
        $error[]    = "Nama gedung harus dipilih";
    }
    else {
        try {
            if ($mastering->updateRoom($nama_ruang, $id_gedung, $updated_at, $id_ruang)) {
                
            }
            $mastering->redirect($baseUrl . 'index.php?page=home&action=mroom&updated');
        } catch(Exception $e) {
            echo $e->getMessage();
        }
    }
}

include 'apps/views/layouts/header.view.php';
include 'apps/views/layouts/menu.view.php';
include 'apps/views/mastering/mroom/update.view.php';
include 'apps/views/layouts/footer.view.php';