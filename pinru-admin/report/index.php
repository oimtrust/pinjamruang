<?php
if (defined('RESTRICTED')) {
} else {
    exit('No direct script access allowed!');
}

/**
 * For showing error 500
 */

// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
// error_reporting(E_ALL);


$admin_login = "";

//if not logged in
if (!isset($_SESSION['username'])) {
    $connect->redirect($baseUrl . "index.php?page=auth&action=login");
    exit;
}

//if logged in
$admin_login = "{$_SESSION['username']}";

//to retrive user data
$admin = $connect->execute("SELECT * FROM tbl_admin WHERE username = '{$admin_login}'");

$acara_query = $connect->execute("select acara.id_acara, acara.nama_acara, count(*) as total from tbl_pinjaman as pinjam left join tbl_acara as acara on pinjam.id_acara = acara.id_acara group by acara.id_acara having (count(acara.id_acara) > 0)");
$total_acara = $connect->execute("select acara.id_acara, acara.nama_acara, count(*) as total from tbl_pinjaman as pinjam left join tbl_acara as acara on pinjam.id_acara = acara.id_acara group by acara.id_acara having (count(acara.id_acara) > 0)");

$ruang_query = $connect->execute("select
    ruang.id_ruang, ruang.nama_ruang, count(*) as total
from
    tbl_pinjaman as pinjam left join tbl_ruang as ruang on pinjam.id_ruang = ruang.id_ruang group by ruang.id_ruang having (count(ruang.id_ruang) > 0)");
$total_ruang = $connect->execute("select
    ruang.id_ruang, ruang.nama_ruang, count(*) as total
from
    tbl_pinjaman as pinjam left join tbl_ruang as ruang on pinjam.id_ruang = ruang.id_ruang group by ruang.id_ruang having (count(ruang.id_ruang) > 0)");

include 'apps/views/layouts/header.view.php';
include 'apps/views/layouts/menu.view.php';
include 'apps/views/report/index.view.php';
include 'apps/views/layouts/footer.view.php';
