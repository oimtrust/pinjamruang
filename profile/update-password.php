<?php
/**
 * Created by PhpStorm.
 * User: dot
 * Date: 4/1/18
 * Time: 2:14 PM
 */

if (defined('RESTRICTED')) {
} else {
    exit('No direct script access allowed!');
}

//error_reporting(0);

$user_login = "";

//if not logged in
if (!isset($_SESSION['username'])) {
    $connect->redirect($baseUrl . "index.php?page=auth&action=login");
    exit;
}

//if logged in
$user_login = "{$_SESSION['username']}";

//to retrive user data
$user = $connect->execute("SELECT * FROM tbl_peminjam AS peminjam LEFT JOIN tbl_instansi AS instansi ON peminjam.id_instansi = instansi.id_instansi WHERE username = '{$user_login}'");

$profile = $user->fetch_object();

if (isset($_GET['update_id']) && !empty($_GET['update_id'])) {
    $id_pinjam = $_GET['update_id'];
    $stmt = $connect->execute("SELECT
            id_peminjam
            FROM tbl_peminjam  
            WHERE id_peminjam = '{$id_pinjam}'");
    $detailPinjam = $stmt->fetch_object();
} else {
    $connect->redirect($baseUrl . 'index.php?page=home&action=profile');
}

if (isset($_POST['btn_update'])) {
    $id_peminjam        = $_POST['id_peminjam'];
    $old_password       = md5($_POST['old_password']);
    $new_password       = md5($_POST['new_password']);
    $confirm_password   = md5($_POST['confirm_password']);

    $queryForViewingPassword    = $auth->execute("SELECT `password` FROM tbl_peminjam WHERE id_peminjam = '{$id_peminjam}'");
    $selectPassword             = $queryForViewingPassword->fetch_object();

    if ($selectPassword->password != $old_password)
    {
        $error[]    = "Password lama anda salah";
    }
    elseif ($old_password == '')
    {
        $error[]    = "Password lama masih kosong";
    }
    elseif ($new_password == '')
    {
        $error[]    = "Password baru masih kosong";
    }
    elseif ($confirm_password == '')
    {
        $error[]    = "Konfirmasi password masih kosong";
    }
    elseif ($new_password != $confirm_password)
    {
        $error[]    = "Password baru harus sama dengan Konfirmasi password";
    }
    elseif ($new_password == $confirm_password)
    {
        try {
            if ($auth->updateUserPassword($new_password, $id_peminjam))
            {

            }
            $auth->redirect($baseUrl.'index.php?page=home&action=profile&updated');
        }
        catch (Exception $e)
        {
            echo $e->getMessage();
        }
    }
}

include "apps/views/layouts/header.view.php";
include "apps/views/layouts/menu.view.php";
include "apps/views/profile/update-password.view.php";
include "apps/views/layouts/footer.view.php";