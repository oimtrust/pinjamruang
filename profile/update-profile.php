<?php
/**
 * Created by PhpStorm.
 * User: dot
 * Date: 3/11/18
 * Time: 8:36 PM
 */

if (defined('RESTRICTED')) {
} else {
    exit('No direct script access allowed!');
}

//error_reporting(0);

$user_login = "";

//if not logged in
if (!isset($_SESSION['username'])) {
    $connect->redirect($baseUrl . "index.php?page=auth&action=login");
    exit;
}

//if logged in
$user_login = "{$_SESSION['username']}";

//to retrive user data
$user = $connect->execute("SELECT * FROM tbl_peminjam WHERE username = '{$user_login}'");

$profile = $user->fetch_object();

if (isset($_GET['update_id']) && !empty($_GET['update_id'])) {
    $id_pinjam = $_GET['update_id'];
    $stmt = $connect->execute("SELECT
            *
            FROM tbl_peminjam AS peminjam 
            LEFT JOIN tbl_instansi AS instansi ON peminjam.id_instansi = instansi.id_instansi 
            WHERE peminjam.id_peminjam = '{$id_pinjam}'");
    $detailPinjam = $stmt->fetch_object();
} else {
    $connect->redirect($baseUrl . 'index.php?page=home&action=profile');
}

if (isset($_POST['btn_update']))
{
    $id_peminjam    = $_POST['id_peminjam'];
    $username       = $_POST['username'];
    $fullname       = $_POST['fullname'];
    $phone          = $_POST['phone'];
    $email          = $_POST['email'];
    $address        = $_POST['address'];
    $status         = $_POST['status'];
    $id_instansi    = $_POST['id_instansi'];

    if ($username == '')
    {
        $error[]    = "Username masih kosong";
    }
    elseif ($fullname == '')
    {
        $error[]    = "Nama masih kosong";
    }
    elseif ($phone == '')
    {
        $error[]    = "No. Telp masih kosong";
    }
    elseif ($address == '')
    {
        $error[]    = "Alamat masih kosong";
    }
    elseif (empty($status))
    {
        $error[]    = "Status masih kosong";
    }
    elseif (empty($id_instansi))
    {
        $error[]    = "Instansi masih kosong";
    }
    else {
        try {
            if ($auth->updateUserProfile($username, $fullname, $phone, $email, $address, $status, $id_instansi, $id_peminjam)) {

            }
            $auth->redirect($baseUrl.'index.php?page=home&action=profile&updated');
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }
}

include "apps/views/layouts/header.view.php";
include "apps/views/layouts/menu.view.php";
include "apps/views/profile/update-profile.view.php";
include "apps/views/layouts/footer.view.php";